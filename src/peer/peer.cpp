#include "address.hpp" // PRAGMA iwyu: keep
#include "connection.hpp"
#include "message.hpp"
#include "peer.hpp"
#include "protocols/ipv4.hpp"
#include "protocols/ipv6.hpp"
#include "protocols/onion.hpp"
#include "site/site.hpp"
#include "util/blobmsgpack.hpp"
#include <cassert>
#include <iostream>
#include <msgpack.hpp>
#include <ofats/invocable.h>
// PRAGMA iwyu: no_include "protocols/../peer/address.hpp"


Peer::Peer(PeerAddress addr, std::string origin): addr(std::move(addr)), origin(std::move(origin)) {
}


void Peer::connect() {
	// clang-tidy doesn't work well with assert
	assert(is_connectable()); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)

	if(is_connecting()) {
		return;
	}

	//std::cout << "Connecting to " << addr.to_string() << std::endl;
	auto conn = connection_manager.add(addr);
	emitter.publish(PeerConnectingEvent{});
	auto this_ptr = std::weak_ptr(shared_from_this());
	conn->emitter.once<ConnectionEstablishedEvent>([this_ptr](const ConnectionEstablishedEvent&, ConnectionEmitter&) {
		if(this_ptr.expired()) {
			return;
		}
		auto ptr = this_ptr.lock();
		ptr->emitter.publish(PeerConnectedEvent{});
	});
	conn->emitter.once<ConnectionClosedEvent>([this_ptr](const ConnectionClosedEvent&, ConnectionEmitter&) {
		if(this_ptr.expired()) {
			return;
		}
		this_ptr.lock()->emitter.publish(PeerDisconnectedEvent{});
	});
}


bool Peer::is_connectable() const {
	return addr.is_connectable();
}


bool Peer::is_connecting() const {
	auto conn = connection_manager.get(addr);
	return !conn.expired();
}


bool Peer::is_connected() const {
	auto conn = connection_manager.get(addr);
	return !conn.expired() && conn.lock()->established;
}


void Peer::add_task(std::shared_ptr<TaskHandle> handle) {
	assert(current_task.expired()); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
	current_task = handle;
	handle->add_peer(std::weak_ptr(shared_from_this()));
}


void Peer::pex(const std::weak_ptr<Site>& site, ofats::any_invocable<void(std::vector<PeerAddress>)>&& callback) {
	auto conn = connection_manager.get(addr);
	if(conn.expired() || site.expired()) {
		return;
	}
	auto conn_ptr = conn.lock();
	conn_ptr->send(ConnectionMessagePex{
		"pex",
		conn_ptr->get_req_id(),
		ConnectionPex{
			site.lock()->get_case_sensitive_address(),
			{},
			{},
			{},
			30
		}
	}, [callback = std::move(callback)](msgpack::object msg) mutable {
		try {
			auto pex = msg.as<ConnectionMessagePexResponse>();
			std::vector<PeerAddress> peers;
			peers.insert(peers.end(), std::make_move_iterator(pex.peers.begin()), std::make_move_iterator(pex.peers.end()));
			peers.insert(peers.end(), std::make_move_iterator(pex.peers_ipv6.begin()), std::make_move_iterator(pex.peers_ipv6.end()));
			peers.insert(peers.end(), std::make_move_iterator(pex.peers_onion.begin()), std::make_move_iterator(pex.peers_onion.end()));
			callback(peers);
		} catch(msgpack::v1::type_error& e) {
			std::cout << "invalid message " << e.what() << std::endl;
		}
	});
}


void Peer::list_modified(const std::weak_ptr<Site>& site, int since, ofats::any_invocable<void(std::map<std::string, int>)>&& callback) {
	auto conn = connection_manager.get(addr);
	if(conn.expired() || site.expired()) {
		return;
	}
	auto conn_ptr = conn.lock();
	conn_ptr->send(ConnectionMessageListModified{
		"listModified",
		conn_ptr->get_req_id(),
		ConnectionListModified{
			site.lock()->get_case_sensitive_address(),
			since
		}
	}, [callback = std::move(callback)](msgpack::object&& msg) mutable {
		try {
			auto modified = msg.as<ConnectionMessageListModifiedResponse>();
			callback(modified.modified_files);
		} catch(msgpack::v1::type_error& e) {
			std::cout << "invalid message " << e.what() << std::endl;
		}
	});
}


void Peer::read_file(const std::weak_ptr<Site>& site, const std::string& path, size_t location, size_t limit, std::optional<size_t> size, ofats::any_invocable<void(std::optional<Blob>)>&& callback) {
	auto conn = connection_manager.get(addr);
	if(conn.expired() || site.expired()) {
		return;
	}
	auto conn_ptr = conn.lock();

	conn_ptr->send(ConnectionMessageReadFile{
		"getFile",
		conn_ptr->get_req_id(),
		ConnectionReadFile{
			site.lock()->get_case_sensitive_address(),
			path,
			location,
			limit,
			size
		}
	}, [callback = std::move(callback)](msgpack::object&& msg) mutable {
		try {
			auto file = msg.as<ConnectionMessageReadFileResponse>();
			callback(file.body);
		} catch(msgpack::v1::type_error& e) {
			std::cout << "invalid message " << e.what() << std::endl;
		}
	});
}


void Peer::stream_file(const std::weak_ptr<Site>& site, const std::string& path, const std::shared_ptr<std::ofstream>& stream, std::optional<size_t> size, size_t location, ofats::any_invocable<void(bool)>&& callback) {
	auto this_ptr = std::weak_ptr(shared_from_this());
	read_file(site, path, location, 131072, size, [=, callback = std::move(callback)](std::optional<Blob> blob) mutable {
		if(!blob || this_ptr.expired()) {
			return;
		}
		// Unnecessary nolint is tracked by https://bugs.llvm.org/show_bug.cgi?id=45786
		// NOLINTNEXTLINE(clang-analyzer-cplusplus.InnerPointer, cppcoreguidelines-pro-type-reinterpret-cast)
		stream->write(reinterpret_cast<char*>(blob->data()), blob->size());
		if(!stream->good()) {
			callback(false);
			return;
		}
		if(blob->empty()) {
			stream->close();
			callback(true);
		} else {
			this_ptr.lock()->stream_file(site, path, stream, size, location + blob->size(), std::move(callback));
		}
	});
}


TaskHandle::TaskHandle(std::string subject, const std::function<void(const std::shared_ptr<Peer>&)>& deleter): deleter(deleter), subject(subject) {
}


void TaskHandle::add_peer(std::weak_ptr<Peer> peer) {
	peers.push_back(peer);
}


void TaskHandle::mark_completed() {
	completed = true;
	for(auto& peer: peers) {
		if(!peer.expired()) {
			auto ptr = peer.lock();
			ptr->current_task.reset();
			deleter(ptr);
		}
	}
	peers.clear();
}


bool TaskHandle::is_completed() const {
	return completed;
}