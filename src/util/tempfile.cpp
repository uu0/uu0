#include "rand.hpp"
#include "tempfile.hpp"


TempFile::TempFile() {
	std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	std::uniform_int_distribution<size_t> rnd(0, alphabet.size() - 1);

	std::string filename;
	for(int i = 0; i < 10; i++) {
		filename.push_back(alphabet[rnd(random_source::gen)]);
	}
	path = std::filesystem::temp_directory_path() / filename;
	existing = true;
}


TempFile::TempFile(TempFile&& other) noexcept {
	path = other.path;
	existing = true;
	other.existing = false;
}


TempFile& TempFile::operator=(TempFile&& other) noexcept {
	if(existing) {
		remove();
	}
	path = other.path;
	existing = true;
	other.existing = false;
	return *this;
}


const std::filesystem::path& TempFile::get_path() const {
	return path;
}


TempFile::~TempFile() {
	remove();
}


void TempFile::remove() {
	if(!existing) {
		return;
	}
	std::error_code err;
	std::filesystem::remove(path, err);
}


bool TempFile::move(const std::filesystem::path& to) {
	if(!existing) {
		return false;
	}
	existing = false;
	std::error_code err;
	std::filesystem::rename(path, to, err);
	if(!err) {
		return true;
	}
	if(err.default_error_condition() == std::errc::cross_device_link) {
		// Moving the file didn't work because the temporary file was saved to the wrong filesystem.
		// TODO(ivanq): Try saving to the same filesystem where the data is to be placed to minimize performance loss
		std::filesystem::copy(path, to, err);
		bool ok = static_cast<bool>(err);
		std::filesystem::remove(path, err);
		ok = ok && err;
		return ok;
	}
	return false;
}