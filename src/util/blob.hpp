#ifndef UTIL_BLOB_HPP
#define UTIL_BLOB_HPP


using Blob = std::basic_string<unsigned char>;


inline Blob operator""_blob(const char* data, size_t size) {
	std::string str(data, size);
	Blob res(size, 0);
	std::copy(str.begin(), str.end(), res.begin());
    return res;
}


#endif