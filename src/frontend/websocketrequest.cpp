#include "httprequest.hpp"
#include "websocketrequest.hpp"
#include <websocket-parser/websocket_parser.h>


WebSocketRequest::WebSocketRequest(HTTPRequest& http): http(http) {
}


void WebSocketRequest::send(websocket_flags flags, const char* data, size_t size) const {
	auto frame_len = websocket_calc_frame_size(flags, size);
	// char[] is forced by uvw, wrapping it in unique_ptr is the best we can achieve
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto frame = std::make_unique<char[]>(frame_len);
	websocket_build_frame(frame.get(), flags, nullptr, data, size);
	http.send(std::move(frame), frame_len);
}


// Or'ing signed integers is forced by websocket_parser
void WebSocketRequest::send(const std::string& data) const {
	send(WS_OP_TEXT | WS_FIN, data.data(), data.size()); // NOLINT(hicpp-signed-bitwise)
}


void WebSocketRequest::send_pong(const std::string& data) const {
	send(WS_OP_PONG | WS_FIN, data.data(), data.size()); // NOLINT(hicpp-signed-bitwise)
}


void WebSocketRequest::send_close() const {
	send(WS_OP_CLOSE | WS_FIN, nullptr, 0); // NOLINT(hicpp-signed-bitwise)
}