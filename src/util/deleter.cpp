void free_deleter(void* ptr) {
	// This function acts as a wrapper around std::free that has an address
	std::free(ptr); // NOLINT(cppcoreguidelines-owning-memory, cppcoreguidelines-no-malloc)
}