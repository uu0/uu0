#include "base58.hpp"
#include "bignumber.hpp"
#include "blob.hpp"
#include "hash.hpp"
#include <openssl/bn.h>


std::string base58::encode(const Blob& data) {
	size_t i;
	for(i = 0; i < data.size() && data[i] == 0; i++) {
	}
	BigNumber bn(data.substr(i));
	std::string alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
	std::string encoded;
	while(bn > 0x00_bn) {
		encoded.push_back(alphabet[static_cast<BN_ULONG>(bn % 0x3a_bn)]);
		bn = bn / 0x3a_bn;
	}
	std::reverse(encoded.begin(), encoded.end());
	return std::string(i, '1') + encoded;
}


std::string base58::encode_check(const Blob& data) {
	auto sha256_1 = sha256(data.data(), data.size());
	auto sha256_2 = sha256(sha256_1.data(), sha256_1.size());
	auto checksum = Blob(sha256_2.data(), sha256_2.size()).substr(0, 4);
	return base58::encode(data + checksum);
}