#ifndef SITE_MANAGER_HPP
#define SITE_MANAGER_HPP


#include <map>
class Site;


class SiteManager {
	std::map<std::string, std::shared_ptr<Site>> sites;

public:
	std::weak_ptr<Site> get(const std::string& address);
	std::weak_ptr<Site> add(const std::string& address);
	[[nodiscard]] std::vector<std::string> get_site_list() const;
};


extern SiteManager site_manager;


#endif