#ifndef ANNOUNCER_HTTP_HPP
#define ANNOUNCER_HTTP_HPP


#include "peer/address.hpp" // PRAGMA iwyu: keep
// PRAGMA iwyu: no_include "protocols/../peer/address.hpp"


void announce_http(std::array<unsigned char, 20> sha1_raw, int numwant, std::array<char, 20> peer_id, sockaddr addr, const std::string& hostname, const std::string& port, const std::string& hostpath, const std::function<void(std::vector<PeerAddress>)>& callback);


#endif