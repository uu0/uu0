#ifndef FRONTEND_HTTPREQUEST_HPP
#define FRONTEND_HTTPREQUEST_HPP


#include "util/blob.hpp"
#include <filesystem>
#include <map>
#include <sstream>


class HTTPRequest {
	std::shared_ptr<uvw::TCPHandle> tcp;

	static std::string extension_to_mime(std::string ext);


public:
	std::string method;
	std::string url;
	std::map<std::string, std::string> headers;

	explicit HTTPRequest(std::shared_ptr<uvw::TCPHandle> tcp);


	template<typename T> void send(T&& data, size_t count) {
		tcp->write(std::forward<T>(data), count);
	}
	void send(const Blob& data);
	void send(Blob&& data);
	void send(const std::string& data);
	void send(std::string&& data);


	template<typename T> void send_chunk(T&& data, size_t count) {
		// Prefix: count in hex + "\r\n"
		std::stringstream ss;
		ss << std::hex << std::uppercase << count;
		auto prefix = ss.str() + "\r\n";
		// char[] is forced by uvw, wrapping it in unique_ptr is the best we can achieve
		// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
		auto prefix_ptr = std::make_unique<char[]>(prefix.size());
		std::copy(prefix.begin(), prefix.end(), prefix_ptr.get());
		send(std::move(prefix_ptr), prefix.size());

		// Real data
		send(std::forward<T>(data), count);

		// Suffix: "\r\n"
		send("\r\n"_blob);
	}

	void send_chunk(const Blob& data);
	void send_chunk(Blob&& data);
	void send_chunk(const std::string& data);
	void send_chunk(std::string&& data);


	void send_head(const std::string& status, const std::map<std::string, std::string>& headers);
	void end();
	void close();
	void send_file(const std::filesystem::path& path);
	void output_file(const std::filesystem::path& path);
};


#endif