PREFIX =
CXX = clang++
CC = clang
TARGET = unknown

_COMMONOPTS := -Wall -Wno-missing-braces -Wextra -Wformat=2 -Wformat-overflow=2 -Wformat-signedness -Wformat-truncation=2 -Wnull-dereference -Wimplicit-fallthrough=5 -Wmissing-include-dirs -Wswitch-default -Wuninitialized -Wstrict-overflow=4 -Wstringop-overflow=4 -Wstringop-truncation -Wsuggest-attribute=pure -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-attribute=malloc -Wsuggest-attribute=format -Wsuggest-attribute=cold -Wsuggest-final-types -Wsuggest-final-methods -Wsuggest-override -Walloc-zero -Wattribute-alias=2 -Wduplicated-branches -Wduplicated-cond -Wtrampolines -Wfloat-equal -Wshadow -Wstack-usage=1024 -Wunsafe-loop-optimizations -Waligned-new=all -Wplacement-new=2 -Wpointer-arith -Wundef -Wunused-macros -Wcast-qual -Wcast-align=strict -Wconditionally-supported -Wconversion -Wzero-as-null-pointer-constant -Wdate-time -Wuseless-cast -Wextra-semi -Wsign-conversion -Wlogical-op -Wno-aggressive-loop-optimizations -Wpadded -Wlong-long -Wvla -Wdisabled-optimization -Wstack-protector -O2 -target $(TARGET)
_CXXOPTS := $(CXXOPTS) $(_COMMONOPTS)
_CCOPTS := $(CCOPTS) $(_COMMONOPTS)
_LINKOPTS := $(LINKOPTS) $(_COMMONOPTS)
ifeq ($(DEBUG), 1)
	_CXXOPTS := $(_CXXOPTS) -g
	_CCOPTS := $(_CCOPTS) -g
	_LINKOPTS := $(_LINKOPTS) -g
else
	_CXXOPTS := $(_CXXOPTS) -fmerge-all-constants -fno-ident -ffunction-sections -fdata-sections
	_CCOPTS := $(_CCOPTS) -fmerge-all-constants -fno-ident -ffunction-sections -fdata-sections
	_LINKOPTS := $(_LINKOPTS) -s -Wl,--gc-sections -Wl,-z,norelro
endif
ifeq ($(SANITIZE), 1)
	_CXXOPTS := $(_CXXOPTS) -fsanitize=bounds,address,undefined
	_CCOPTS := $(_CCOPTS) -fsanitize=bounds,address,undefined
	_LINKOPTS := $(_LINKOPTS) -fsanitize=bounds,address,undefined
endif


SRCS = $(shell find src -name "*.cpp")
HEADERS = $(shell find include/ -name "*.hpp")
OBJS = $(SRCS:.cpp=.o)
DEPS = $(SRCS:.cpp=.d)

HP_SRCS = vendor/http-parser/http_parser.c
HP_OBJS = $(HP_SRCS:.c=.o)
HP_DEPS = $(HP_SRCS:.c=.d)

UVW_SRCS = $(shell find library/uvw/src -name "*.cpp")
UVW_OBJS = $(UVW_SRCS:.cpp=.o)
UVW_DEPS = $(UVW_SRCS:.cpp=.d)

LIBS = library/libuv/.libs/libuv.a library/websocket-parser/libwebsocket_parser.a
INCLUDES = -isystem vendor -isystem library $(foreach dir,$(wildcard vendor/*/include library/*/include),-isystem $(dir)) -iquote src

resolve = $(shell echo "\#include <$(1)>" | $(CXX) -H -E -x c++ - 2>&1 >/dev/null | head -1 | tail -c+3)


SRCS_IWYU = $(SRCS:.cpp=.iwyu)
HEADERS_IWYU = $(HEADERS:.hpp=.hiwyu)
IVYUOPTS = -Xiwyu --no_fwd_decls -Xiwyu --prefix_header_includes=remove -Xiwyu --mapping_file=iwyu-mapping.imp -include uvw/src/uvw.hpp $(INCLUDES) --std=c++17


SRCS_LINT = $(SRCS:.cpp=.lint)
HEADERS_LINT = $(HEADERS:.hpp=.hlint)


.PHONY: clean iwyu clean-iwyu lint clean-lint clean-uvw $(LIBS)

all: uu0 $(LIBS)

uu0: $(OBJS) $(HP_OBJS) $(UVW_OBJS)
	$(PREFIX) $(CXX) -o uu0 $(OBJS) $(HP_OBJS) $(UVW_OBJS) $(LIBS) -ldl -lcrypto -lpthread $(_LINKOPTS)

$(OBJS): %.o: %.cpp library/uvw/src/uvw.hpp.gch
	$(PREFIX) $(CXX) -Winvalid-pch -DUVW_AS_LIB -include-pch library/uvw/src/uvw.hpp.gch -MMD -MP -c $< -o $@ --std=c++17 $(_CXXOPTS) $(INCLUDES)

$(HP_OBJS): %.o: %.c
	C_INCLUDE_PATH=vendor/http-parser $(PREFIX) $(CC) -MMD -MP -c $< -o $@ --std=c11 $(_CCOPTS) $(INCLUDES)

clean:
	$(RM) $(OBJS) $(HP_OBJS) $(DEPS) library/uvw/src/uvw.hpp.gch *~ uu0


uvw: library/uvw/src/uvw.hpp.gch $(UVW_OBJS)

%.hpp.gch: %.hpp
	$(PREFIX) $(CXX) -x c++-header -DUVW_AS_LIB -MMD -MP -c $< -o $@ --std=c++17 $(_CXXOPTS) -Ilibrary/libuv/include

$(UVW_OBJS): %.o: %.cpp
	$(PREFIX) $(CXX) -DUVW_AS_LIB -MMD -MP -c $< -o $@ --std=c++17 $(_CXXOPTS) -Ilibrary/libuv/include

clean-uvw:
	$(RM) $(UVW_OBJS) library/uvw/src/uvw.hpp.gch $(UVW_DEPS)


iwyu: $(SRCS_IWYU) $(HEADERS_IWYU)

$(SRCS_IWYU): %.iwyu: %.cpp
	iwyu $(IVYUOPTS) $< 2>&1 | tee $@

$(HEADERS_IWYU): %.hiwyu: %.hpp
	iwyu $(IVYUOPTS) $< 2>&1 | tee $@

clean-iwyu:
	$(RM) $(SRCS_IWYU) $(HEADERS_IWYU)


lint: $(SRCS_LINT) $(HEADERS_LINT)

$(SRCS_LINT): %.lint: %.cpp
	$(PREFIX) clang-tidy $< -- -fcolor-diagnostics $(INCLUDES) --std=c++17 -include uvw/src/uvw.hpp -target $(TARGET) $(LINTOPTS) 2>&1 | tee $@

$(HEADERS_LINT): %.hlint: %.hpp
	$(PREFIX) clang-tidy $< -- -fcolor-diagnostics $(INCLUDES) --std=c++17 -include uvw/src/uvw.hpp -target $(TARGET) $(LINTOPTS) 2>&1 | tee $@

clean-lint:
	$(RM) $(SRCS_LINT) $(HEADERS_LINT)


library/libuv/.libs/libuv.a:
	$(MAKE) -C library/libuv all

library/websocket-parser/libwebsocket_parser.a:
	$(MAKE) -C library/websocket-parser alib


-include $(DEPS) $(HP_DEPS) $(UVW_DEPS)
