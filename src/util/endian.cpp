uint64_t ntohq(uint64_t num) {
	auto low = ntohl(num & 0xFFFFFFFFU);
	auto high = ntohl(num >> 32U);
	if(ntohs(1) != 1) {
		std::swap(low, high);
	}
	return low | (static_cast<uint64_t>(high) << 32U);
}


uint64_t htonq(uint64_t num) {
	return ntohq(num);
}