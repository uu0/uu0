#include "frontend/httprequest.hpp"
#include "frontend/websockethandler.hpp"
#include "frontend/websocketrequest.hpp"
#include "http.hpp"
#include "router.hpp"
#include "util/base64.hpp"
#include "util/blob.hpp"
#include "util/hash.hpp"
#include <http-parser/http_parser.h>
#include <websocket-parser/websocket_parser.h>


void HTTPServer::listen(const std::string& ip, int port) { // NOLINT(readability-convert-member-functions-to-static)
	auto loop = uvw::Loop::getDefault();

	auto srv = loop->resource<uvw::TCPHandle>();
	srv->on<uvw::ListenEvent>([](const uvw::ListenEvent&, uvw::TCPHandle& srv) {
		auto tcp = srv.loop().resource<uvw::TCPHandle>();

		auto parser = std::make_shared<http_parser>();
		http_parser_init(parser.get(), HTTP_REQUEST);

		auto state = std::make_shared<HTTPClientState>(HTTPClientState{tcp, HTTPRequest{tcp}});
		parser->data = state.get();

		auto settings = std::make_shared<http_parser_settings>();
		settings->on_message_begin = [](http_parser*) {
			return 0;
		};
		settings->on_headers_complete = [](http_parser* parser) {
			auto state = static_cast<HTTPClientState*>(parser->data);
			if(state->header_state == HTTPClientState::HeaderState::value) {
				state->request.headers[state->last_header_field] = state->last_header_value;
			}
			return 0;
		};
		settings->on_message_complete = [](http_parser* parser) {
			auto state = static_cast<HTTPClientState*>(parser->data);
			if(!parser->upgrade) {
				FrontendRouter(state->request).route();
			}
			return 0;
		};
		settings->on_chunk_header = [](http_parser*) {
			return 0;
		};
		settings->on_chunk_complete = [](http_parser*) {
			return 0;
		};
		settings->on_url = [](http_parser* parser, const char* at, size_t length) {
			auto state = static_cast<HTTPClientState*>(parser->data);
			state->request.url = std::string(at, length);
			state->request.method = http_method_str(static_cast<http_method>(parser->method));
			return 0;
		};
		settings->on_status = [](http_parser*, const char*, size_t) {
			return 0;
		};
		settings->on_header_field = [](http_parser* parser, const char* at, size_t length) {
			auto state = static_cast<HTTPClientState*>(parser->data);
			if(state->header_state == HTTPClientState::HeaderState::none) {
				state->last_header_field = std::string(at, length);
			} else if(state->header_state == HTTPClientState::HeaderState::field) {
				state->last_header_field += std::string(at, length);
			} else if(state->header_state == HTTPClientState::HeaderState::value) {
				state->request.headers[state->last_header_field] = state->last_header_value;
				state->last_header_field = std::string(at, length);
			}
			state->header_state = HTTPClientState::HeaderState::field;
			return 0;
		};
		settings->on_header_value = [](http_parser* parser, const char* at, size_t length) {
			auto state = static_cast<HTTPClientState*>(parser->data);
			if(state->header_state == HTTPClientState::HeaderState::field) {
				state->last_header_value = std::string(at, length);
			} else if(state->header_state == HTTPClientState::HeaderState::value) {
				state->last_header_value += std::string(at, length);
			}
			state->header_state = HTTPClientState::HeaderState::value;
			return 0;
		};
		settings->on_body = [](http_parser*, const char*, size_t) {
			return 0;
		};

		tcp->on<uvw::DataEvent>([parser, settings, state](const uvw::DataEvent& data, uvw::TCPHandle& tcp) {
			if(parser->upgrade) {
				if(websocket_parser_execute(&state->ws->parser, &state->ws->settings, data.data.get(), data.length) != data.length) {
					// Error
					tcp.close();
				}
			} else {
				auto nparsed = http_parser_execute(
					parser.get(),
					settings.get(),
					data.data.get(),
					data.length
				);
				if(parser->upgrade) {
					if(state->request.headers.count("Sec-WebSocket-Key") == 0) {
						state->request.send_head("400 Bad Request", {
							{"Content-Type", "text/html"},
							{"Transfer-Encoding", "chunked"}
						});
						state->request.send_chunk("<h1>400 Bad Request</h1><p>Missing Sec-WebSocket-Key header</p>");
						state->request.end();
						tcp.close();
						return;
					}

					auto subject = state->request.headers["Sec-WebSocket-Key"] + WEBSOCKET_UUID;
					auto sha1_raw = sha1(subject.data(), subject.size());
					auto accept = base64::encode(Blob(sha1_raw.begin(), sha1_raw.end()));

					state->request.send_head("101 Switching Protocols", {
						{"Upgrade", "websocket"},
						{"Connection", "Upgrade"},
						{"Sec-WebSocket-Accept", accept}
					});

					state->ws = std::make_unique<WebSocketClientState>(state->request);
					state->ws->settings.on_frame_header = [](websocket_parser* parser) {
						auto state = static_cast<WebSocketClientState*>(parser->data);
						if(parser->length != 0) {
							state->frame_bodies.emplace_back(parser->length, '\0');
						}
						// And'ing signed values is forced by websocket_parser
						if((parser->flags & WS_OP_MASK) != WS_OP_CONTINUE) { // NOLINT(hicpp-signed-bitwise)
							state->frame_op = parser->flags & WS_OP_MASK; // NOLINT(hicpp-signed-bitwise)
						}
						return 0;
					};
					state->ws->settings.on_frame_body = [](websocket_parser* parser, const char* at, size_t length) {
						auto state = static_cast<WebSocketClientState*>(parser->data);
						auto& body = state->frame_bodies.back();
						auto it = body.begin() + parser->offset;
						if((parser->flags & WS_HAS_MASK) != 0) { // NOLINT(hicpp-signed-bitwise)
							if(it != body.end()) {
								// Dereferencing .end() is UB
								websocket_parser_decode(&*it, at, length, parser);
							}
						} else {
							// I know what I am doing
							// NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
							std::copy(at, at + length, it);
						}
						return 0;
					};
					state->ws->settings.on_frame_end = [](websocket_parser* parser) {
						auto state = static_cast<WebSocketClientState*>(parser->data);
						if((parser->flags & WS_FIN) != 0) { // NOLINT(hicpp-signed-bitwise)
							// Merge all frames into one
							size_t total_size = 0;
							for(auto& str: state->frame_bodies) {
								total_size += str.size();
							}
							std::string body;
							body.reserve(total_size);
							for(auto& str: state->frame_bodies) {
								body += str;
								str.clear(); // deallocate memory as soon as possible
							}

							if(state->frame_op == WS_OP_TEXT || state->frame_op == WS_OP_BINARY) {
								state->handler->handle(body);
							} else if(state->frame_op == WS_OP_PING) {
								state->request.send_pong(body);
							} else if(state->frame_op == WS_OP_CLOSE) {
								state->request.send_close();
 								return 1;
							}
							state->frame_bodies.clear();
						}
						return 0;
					};
					state->ws->parser.data = state->ws.get();

					state->ws->handler.emplace(state->ws->request);

					auto len = data.length - nparsed;
					if(websocket_parser_execute(&state->ws->parser, &state->ws->settings, data.data.get() + nparsed, len) != len) {
						// Error
						tcp.close();
					}
				}
			}
		});

		tcp->on<uvw::CloseEvent>([state](const uvw::CloseEvent&, uvw::TCPHandle& tcp) {
			tcp.clear();
		});
		tcp->on<uvw::EndEvent>([](const uvw::EndEvent&, uvw::TCPHandle& tcp) {
			tcp.close();
		});

		srv.accept(*tcp);
		tcp->read();
	});

	srv->bind(ip, port);
	srv->listen();
}


WebSocketClientState::WebSocketClientState(HTTPRequest& http): request(http) {
	websocket_parser_settings_init(&settings);
	websocket_parser_init(&parser);
}


HTTPServer http_server;