#ifndef SITE_CONTENT_HPP
#define SITE_CONTENT_HPP


bool verify_content(const std::string& content, std::vector<std::string> allowed_addresses, int min_valid_count);


#endif