#ifndef UTIL_RAND_HPP
#define UTIL_RAND_HPP


#include <random>


namespace random_source {
	extern std::random_device rd;
	extern std::mt19937 gen;
};


#endif