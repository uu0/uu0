## Code style

There are several code style points preferred in uu0 project you might not have encountered before.

1. No `using namespace`. Good: `std::cout << "Hello, world!";`. Bad: `usinng namespace std; cout << "Hello, world!";`.

2. Use both `#ifndef` as include guard. Use `DIRECTORY_FILE_HPP` name, e.g. `PEER_CONNECTION_HPP`.

3. Use `struct` for POD types or classes which only have properties, a straightforward constructor (e.g. a struct holding integers and constant length strings) and observer methods. `struct`s should always have the same memory layout as if only properties were used (plus padding unless `#pragma pack` is used). All objects with non-straightforward behavour should be defined as `class`. Good: `struct A { int a; int b; }; class B { public: int a; void mutate() { a++; } };`. Bad: `class A { public: int a; int b; }; struct B { int a; void mutate() { a++; } };`.

4. When including a file from the same subdirectory, use `#include "name.hpp"`. When including a file from another subdirectory, use `#include "directory/name.hpp"`. When including a library that has a separate `include` folder, use `#include <name.hpp>`. When including a library which doesn't have an `include` directory, use `#include <libname/directory/name.hpp>`.

5. Use `static` in code, `inline` in includes.

6. If a class has a copy constructor/assignment operator, it should be movable too.

7. Prefer composition over inheritance. In particular, use `class Derived: public Base` only if `Derived` is some kind of `Base`, and `Derived` can do everything that `Base` can do. When in doubt, try Liskov Substitution Principle. Correct: `class Dog: public Animal`. Wrong: `class Circle: public Ellipse`.

8. Prefer putting return type before name, not after, but it's trailing return type is preferred over `declval` or similar. Good: `int random() { return 4; }`. Bad: `auto random() -> int { return 4 };` Good: `template<typename T, typename U> auto operator-(T a, U b) -> decltype(a + b);`. Bad: `template<typename T, typename U> decltype(declval<T&>() + declval<U&>()) operator-(T a, U b);`.

9. *Always* use RAII. Managing resources manually is asking for trouble if someone throws an exception. Good: using `std::ifstream`. Bad: using `fopen` and `fclose`.

10. Use virtual methods instead of `typeid`. Good: `class Base { virtual void foo() = 0; }; class Derived1: public Base { void foo() override { std::cout << 1; } }; class Derived2: public Base { void foo() override { std::cout << 2; } }; void handle(const Base& obj) { std::cout << "It's "; obj.foo(); }`. Bad: `class Base { ... }; class Derived1: public Base { }; class Derived2: public Base { }; void handle(const Base& obj) { std::cout << "It's "; if(typeid(obj) == typeid(Derived1)) { std::cout << 1; } else { std::cout << 2; } }`.

11. Use post-increment for integers and pre-increment for iterators.

12. Prefer `const T&` over `T const&`.

13. Use `int` for small integers (e.g. loop counters). If you need bigger/smaller types, use `intN_t`/`uintN_t`. Good: `for(int i = 0; i < n; i++) { ... }`. Bad: `for(uint32_t i = 0; i < n; i++) { ... }`. Good: `uint16_t port = 2370;`. Bad: `unsigned short port = 2370;`.

14. Manually serialize/deserialize structs to/from binary form. For legacy code that uses `reinterpret_cast<[unsigned] char*>(...)`, use wrap struct definition with `#pragma pack`, use precise types (e.g. `int32_t` instead of `int`), take care of endianness (perhaps in getters/setters), and use `static_assert` to make sure the sizes are correct.

15. Avoid macros when possible. If absolutely required, `#define` them right before usage and `#undef` right after usage. Make sure not to override existing macros by putting a `#ifdef` check. Good:

```cpp
int before;

#ifdef PROJECTNAME_SET_ATTRIBUTE
#error PROJECTNAME_SET_ATTRIBUTE is already defined
#endif
#define PROJECTNAME_SET_ATTRIBUTE(key, value) attrs[key] = value;
PROJECTNAME_SET_ATTRIBUTE(1, 2)
PROJECTNAME_SET_ATTRIBUTE(3, 4)
#undef PROJECTNAME_SET_ATTRIBUTE

int after;
```

Bad:

```cpp
#define SET_ATTRIBUTE(key, value) attrs[key] = value;
int before;
SET_ATTRIBUTE(1, 2)
SET_ATTRIBUTE(3, 4)
int after
#undef SET_ATTRIBUTE
```

16. Use `'\0'` as zero for chars, `0U` for unsigned chars. Use `nullptr` for pointers. Use `0` for integers.

17. Use `auto` for long types (e.g. iterators) and non-obvious types (e.g. type of `std::string::npos`). Never use `auto` for return types (unless it's a lambda). Don't rely on the compiler setting references/cv qualifiers for you. Good: `auto& ref = get_ref();`. Bad: `auto ref = get_ref();`.

18. Using lambdas is encouraged, but you shouldn't use implicit capture, i.e. don't use `[=]` or `[&]`.

19. Prefer `using` over `typedef` and `enum class` over `enum`.

20. Use PascalCase for classes. Use snake_case for variables and namespaces. Use UPPER_CASE for constants. Prefer self-explanatory names. Well known variables such as `i`, `it` and `T` (for templates) are allowed.

21. Use lowercase for file and directory names. Use `.cpp` extension for C++ code, `.hpp` for C++ headers, `.c` for C code, `.h` for C headers. Make sure your file names don't collide with standard headers. Good: `rand.hpp`. Bad: `random.hpp`.

22. Comments should occupy the whole line (excluding indentation). IWYU pragmas are an exception. Use several single-line comments instead of a multiline comment. For things that can be implemented somewhen later use TODO comments like this: `// TODO(yourname): blah`. If a TODO comment is multiline, only add the prefix to the first line. Another allowed keyword is `FIXME` which marks some code that does not always work and should be fixed as soon as possible. Prefer `FIXME` over `XXX`. Use literal comments instead of `NOTE`.

23. Don't use non-ASCII characters in code. If absolutely required, use UTF-8.

24. Use tabs for indentation, spaces for alignment. Use spaces for indentation inside comments.

25. Don't use spaces between identifiers and parentheses. Put a space between a closing paranthesses and an opening curly brace. Use spaces around infix operators, don't use them near prefix/postfix operators. Use parentheses when operator priority is not obvious. Pointer and reference signs are a part of type, not variable. Good:

```cpp
char* ptr;
if((a & (b == 7)) == 2) {
	std::cout << "Awesome!" << std::endl;
	a++;
} else {
	std::cout << "Not great." << std::endl;
}
```

Bad:

```cpp
char *ptr;
if ((a&b==7)==2){
	std::cout<<"Awesome!"<<std::endl;
	a ++;
}else{
	std::cout<<"Not great."<<std::endl;
}
```

26. Always use curly braces around if/for/etc. body. Good: `if(a) { go(); }`. Bad: `if(a) go();`.


## Checking includes

uu0 uses [include-what-you-use](https://github.com/include-what-you-use/include-what-you-use/) to make sure compilation time is as low as possible. Run `./iwyu` to make sure there are no unnecessary includes and all required ones are in place. Your PR will not be accepted if iwyu reports errors. In case iwyu rejects completely correct code, adding pragmas or changing `iwyu-mapping.imp` is acceptable.


## Avoiding code smell

Use `./lint` to call `clang-tidy`. Please minimize the count of warnings. All rules except the following are mandatory and should not be ignored unless the warnings are in system headers:

- `cppcoreguidelines-pro-type-member-init` -- allowed if initialization happens in constructor body;
- `readability-convert-member-functions-to-static` -- allowed if the function is designed to become non-static in near future;
- `hicpp-signed-bitwise` -- can be disabled in case of false positives. Notice that putting an `assert(n >= 0)` or similar doesn't make a signed integer unsigned;
- `cppcoreguidelines-pro-bounds-array-to-pointer-decay` -- can be disabled in false positives, e.g. in `assert`s;
- `cppcoreguidelines-pro-type-reinterpret-cast` -- `reinterpret_cast<[unsigned] char*>` is allowed in system/library function wrappers, because uu0 uses `unsigned char` for binary data, while many libraries use `char` instead. Also, Berkeley sockets require `reinterpret_cast` to/from `sockaddr`, this is allowed too. Casting to other types is forbidden;
- `readability-implicit-bool-conversion` -- allowed in C function wrappers which return an `int` instead of `bool`, e.g. OpenSSL;
- `cppcoreguidelines-owning-memory`, `cppcoreguidelines-no-malloc` -- allowed when managing memory allocated by a C library. When receiving the pointer, *immediately* wrap it into a smart pointer, probably using `free_deleter` as deleter. This function is just a `std::free` wrapper which has an address (`std::free` doesn't), and this is the only place where `std::malloc`/`std::free` are allowed;
- `cppcoreguidelines-pro-type-union-access` -- allowed in library wrappers. Use `std::variant` in C++ code;
- `cppcoreguidelines-pro-bounds-pointer-arithmetic` -- allowed if the bounds were checked before. Notice that `assert` is not a check.

These rules can be changed in case more false positives are found.


## Security

Use `valgrind`, UBSan and ASan. Memory leaks are not allowed, unless they are limited (e.g. a constant number of threads are created which die at exit). However, you can't just allocate memory and not free it when the pointer becomes inaccessible. Using smart pointers is encouraged.