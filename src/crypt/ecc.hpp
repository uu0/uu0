#ifndef CRYPT_ECC_HPP
#define CRYPT_ECC_HPP


#include "util/bignumber.hpp"
#include "util/blob.hpp"
#include <openssl/ec.h>


class ECPoint;


class ECGroup {
public:
	EC_GROUP* ec_group = nullptr;
	size_t public_key_length = 0;
	ECGroup() = default;
	ECGroup(const BigNumber& p, const BigNumber& a, const BigNumber& b);
	ECGroup(const ECGroup&) = delete;
	ECGroup(ECGroup&& other) noexcept;
	~ECGroup();
	ECGroup& operator=(const ECGroup&) = delete;
	ECGroup& operator=(ECGroup&& other) noexcept;
	void set_generator(const ECPoint& point, const BigNumber& n, const BigNumber& h);
	[[nodiscard]] ECPoint multuply_shamir(const BigNumber& coeff_g, const ECPoint& point, const BigNumber& coeff) const;
};


class ECPoint {
public:
	EC_POINT* ec_point = nullptr;
	explicit ECPoint(const ECGroup& group);
	ECPoint(const BigNumber& x, const BigNumber& y, const ECGroup& group);
	ECPoint(const Blob& blob, const ECGroup& group);
	ECPoint(const ECPoint&) = delete;
	ECPoint(ECPoint&& other) noexcept;
	~ECPoint();
	ECPoint& operator=(const ECPoint&) = delete;
	ECPoint& operator=(ECPoint&& other) noexcept;
	[[nodiscard]] BigNumber get_x(const ECGroup& group) const;
	[[nodiscard]] BigNumber get_y(const ECGroup& group) const;
	void set_coords(const BigNumber& x, const BigNumber& y, const ECGroup& group);
	[[nodiscard]] std::string to_string(const ECGroup& group) const;
};


class ECPublicKey {
public:
	ECGroup const& group;
	ECPoint point;
	bool is_compressed;

	ECPublicKey(const ECGroup& group, ECPoint&& point, bool is_compressed);
	[[nodiscard]] std::string to_address() const;
	[[nodiscard]] std::string to_string() const;
};


class CryptECC {
	BigNumber p, n;
	ECGroup group;

public:
	CryptECC();
	ECPublicKey recover_public_key(const Blob& signature, const Blob& hash);
	static Blob double_format(const Blob& subject);
};


extern CryptECC crypt_ecc;


#endif