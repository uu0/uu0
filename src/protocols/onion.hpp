#ifndef PROTOCOLS_ONION_HPP
#define PROTOCOLS_ONION_HPP


#include "peer/address.hpp"
#include "util/blob.hpp"
#include <byteswap.h>


class PeerAddressOnion: public PeerAddressBase {
protected:
	[[nodiscard]] bool is_less(const PeerAddressBase& other) const override {
		auto other_cast = dynamic_cast<const PeerAddressOnion&>(other);
		if(address != other_cast.address) {
			return address < other_cast.address;
		}
		return ntohs(port) < ntohs(other_cast.port);
	}


	[[nodiscard]] bool is_equal(const PeerAddressBase& other) const override {
		auto other_cast = dynamic_cast<const PeerAddressOnion&>(other);
		return address == other_cast.address && ntohs(port) == ntohs(other_cast.port);
	}


	[[nodiscard]] std::unique_ptr<PeerAddressBase> clone() const override {
		return std::make_unique<PeerAddressOnion>(*this);
	}


public:
	std::array<char, 10> address = {};
	in_port_t port = {};


	PeerAddressOnion() = default;


	PeerAddressOnion(std::array<char, 10> address, in_port_t port): address(address), port(port) {
	}


	[[nodiscard]] bool is_connectable() const override {
		return true;
	}


	static PeerAddressOnion from_bittorrent(Blob raw) {
		if(raw.size() != 12) {
			throw std::length_error("Invalid PeerAddress length");
		}
		PeerAddressOnion addr{};
		std::copy(raw.begin(), raw.begin() + 10, addr.address.begin());
		// NOLINTNEXTLINE(hicpp-signed-bitwise)
		addr.port = htons(static_cast<uint16_t>(raw[11]) | (static_cast<uint16_t>(raw[10]) << 8U));
		return addr;
	}


	void decode_msgpack(Blob raw) override {
		*this = from_bittorrent(raw);
		port = bswap_16(port);
	}


	[[nodiscard]] Blob to_msgpack() const override {
		auto native_port = ntohs(port);
		Blob raw(12, 0);
		std::copy(address.begin(), address.end(), raw.begin());
		raw[10] = native_port & 0xffU; // NOLINT(hicpp-signed-bitwise)
		raw[11] = (native_port >> 8U) & 0xffU; // NOLINT(hicpp-signed-bitwise)
		return raw;
	}


	[[nodiscard]] std::string to_string() const override {
		// TODO(ivanq): implement
		return "?.onion:" + std::to_string(ntohs(port));
	}


	void visit(const std::function<void(sockaddr*)>& /*callback*/) const override {
		throw std::logic_error("PeerAddressOnion can't be converted to sockaddr");
	}
};


#endif