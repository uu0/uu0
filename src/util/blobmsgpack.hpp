#ifndef UTIL_BLOBMSGPACK_HPP
#define UTIL_BLOBMSGPACK_HPP


#include "blob.hpp"
#include <msgpack.hpp>


namespace msgpack {
	MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
		namespace adaptor {
			template<> struct convert<Blob> {
				msgpack::object const& operator()(msgpack::object const& obj, Blob& blob) const {
					if(obj.type == msgpack::type::STR) {
						// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
						blob.resize(obj.via.str.size);
						// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access, cppcoreguidelines-pro-bounds-pointer-arithmetic)
						std::copy(obj.via.str.ptr, obj.via.str.ptr + obj.via.str.size, blob.begin());
					} else if(obj.type == msgpack::type::BIN) {
						// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
						blob.resize(obj.via.str.size);
						// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access, cppcoreguidelines-pro-bounds-pointer-arithmetic)
						std::copy(obj.via.bin.ptr, obj.via.bin.ptr + obj.via.bin.size, blob.begin());
					} else {
						throw msgpack::type_error();
					}
					return obj;
				}
			};

			template<> struct pack<Blob> {
				template<typename Stream> packer<Stream>& operator()(msgpack::packer<Stream>& packer, const Blob& blob) const {
					packer.pack_bin(blob.size());
					// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
					packer.pack_bin_body(reinterpret_cast<const char*>(blob.data()), blob.size());
					return packer;
				}
			};

			template<> struct object_with_zone<Blob> {
				void operator()(msgpack::object::with_zone& obj, const Blob& blob) const {
					obj.type = msgpack::type::BIN;
					// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
					obj.via.bin.size = blob.size();
					auto ptr = static_cast<char*>(obj.zone.allocate_align(blob.size()));
					// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
					obj.via.bin.ptr = ptr;
					std::copy(blob.begin(), blob.end(), ptr);
				}
			};
		}
	}
}


#endif