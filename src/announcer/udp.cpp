#include "protocols/ipv4.hpp"
#include "udp.hpp"
#include "util/blob.hpp"
#include "util/endian.hpp"
#include "util/rand.hpp"
#include <cstring>


#pragma pack(push, 1)

class UDPTrackerRequestHeader {
	uint64_t _connection_id;
	uint32_t _action;
	uint32_t _transaction_id;

public:
	UDPTrackerRequestHeader() = default;
	UDPTrackerRequestHeader(uint64_t connection_id, uint32_t action) {
		_connection_id = htonq(connection_id);
		_action = htonl(action);

		std::uniform_int_distribution<uint32_t> rnd(0, std::numeric_limits<uint32_t>::max());
		_transaction_id = rnd(random_source::gen);
	}

	[[nodiscard]] uint64_t connection_id() const {
		return ntohq(_connection_id);
	}
	[[nodiscard]] uint32_t action() const {
		return ntohl(_action);
	}
	[[nodiscard]] uint32_t transaction_id() const {
		return ntohl(_transaction_id);
	}
};


class UDPTrackerResponseHeader {
	uint32_t _action;
	uint32_t _transaction_id;

public:
	[[nodiscard]] uint32_t action() const {
		return ntohl(_action);
	}
	[[nodiscard]] uint32_t transaction_id() const {
		return ntohl(_transaction_id);
	}
};


class UDPTrackerConnectRequest {
public:
	UDPTrackerRequestHeader header;

	explicit UDPTrackerConnectRequest(const UDPTrackerRequestHeader header): header(header) {}
};


struct UDPTrackerConnectResponse {
private:
	UDPTrackerResponseHeader _header;
	uint64_t _connection_id;

public:
	UDPTrackerConnectResponse() = default;
	UDPTrackerConnectResponse(
		UDPTrackerResponseHeader header,
		uint64_t connection_id
	): _header(header) {
		_connection_id = htonq(connection_id);
	}

	[[nodiscard]] UDPTrackerResponseHeader& header() {
		return _header;
	}
	[[nodiscard]] uint64_t connection_id() const {
		return ntohq(_connection_id);
	}
};


struct UDPTrackerAnnounceRequest {
private:
	UDPTrackerRequestHeader _header;
	unsigned char _info_hash[20];
	char _peer_id[20];
	uint64_t _downloaded;
	uint64_t _left;
	uint64_t _uploaded;
	uint32_t _event;
	uint32_t _ip_address;
	uint32_t _key;
	uint32_t _numwant;
	uint16_t _port;

public:
	// _info_hash and _peer_id are initialized by constructor body
	UDPTrackerAnnounceRequest( // NOLINT(cppcoreguidelines-pro-type-member-init)
		UDPTrackerRequestHeader header,
		std::array<unsigned char, sizeof(_info_hash)> info_hash,
		std::array<char, sizeof(_peer_id)> peer_id,
		uint64_t downloaded,
		uint64_t left,
		uint64_t uploaded,
		uint32_t event,
		uint32_t ip_address,
		uint32_t key,
		uint32_t numwant,
		uint16_t port
	): _header(header) {
		std::copy(info_hash.begin(), info_hash.end(), std::begin(_info_hash));
		std::copy(peer_id.begin(), peer_id.end(), std::begin(_peer_id));
		_downloaded = htonq(downloaded);
		_left = htonq(left);
		_uploaded = htonq(uploaded);
		_event = htonl(event);
		_ip_address = htonl(ip_address);
		_key = htonl(key);
		_numwant = htonl(numwant);
		_port = htons(port);
	}

	[[nodiscard]] UDPTrackerRequestHeader& header() {
		return _header;
	}
};


struct UDPTrackerAnnounceResponse {
private:
	UDPTrackerResponseHeader _header;
	uint32_t _interval;
	uint32_t _leachers;
	uint32_t _seeders;

public:
	UDPTrackerAnnounceResponse() = default;
	UDPTrackerAnnounceResponse(
		UDPTrackerResponseHeader header,
		uint32_t interval,
		uint32_t leachers,
		uint32_t seeders
	): _header(header) {
		_interval = htonl(interval);
		_leachers = htonl(leachers);
		_seeders = htonl(seeders);
	}

	[[nodiscard]] UDPTrackerResponseHeader& header() {
		return _header;
	}
	[[nodiscard]] uint64_t interval() const {
		return ntohl(_interval);
	}
	[[nodiscard]] uint64_t leachers() const {
		return ntohl(_leachers);
	}
	[[nodiscard]] uint64_t seeders() const {
		return ntohl(_seeders);
	}
};

#pragma pack(pop)


enum class AnnounceStage {
	connecting,
	announcing
};


void announce_udp(std::array<unsigned char, 20> sha1_raw, int numwant, std::array<char, 20> peer_id, sockaddr addr, const std::string& /*hostname*/, const std::string& /*port*/, const std::string& /*hostpath*/, const std::function<void(std::vector<PeerAddress>)>& callback) {
	auto loop = uvw::Loop::getDefault();

	auto udp = loop->resource<uvw::UDPHandle>();
	auto timer = loop->resource<uvw::TimerHandle>();

	// Connect
	UDPTrackerRequestHeader header(0x41727101980, 0);
	// Forced by uvw
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto req = std::make_unique<char[]>(sizeof(UDPTrackerConnectRequest));
	new(req.get()) UDPTrackerConnectRequest(header);
	auto last_transaction_id = std::make_shared<uint32_t>(header.transaction_id());
	udp->send(addr, std::move(req), sizeof(UDPTrackerConnectRequest));


	auto stage = std::make_shared<AnnounceStage>(AnnounceStage::connecting);

	udp->once<uvw::CloseEvent>([](const uvw::CloseEvent&, uvw::UDPHandle& udp) {
		udp.clear();
	});

	udp->on<uvw::UDPDataEvent>([=](const uvw::UDPDataEvent& data, uvw::UDPHandle& udp) {
		if(*stage == AnnounceStage::connecting) {
			if(data.length != sizeof(UDPTrackerConnectResponse)) {
				return;
			}
			// It's fine, we only read UDPTrackerConnectResponse so no leaks are possible
			UDPTrackerConnectResponse response; // NOLINT(cppcoreguidelines-pro-type-member-init)
			memcpy(&response, data.data.get(), sizeof(response));
			if(response.header().transaction_id() != *last_transaction_id || response.header().action() != 0) {
				return;
			}

			// Announce
			// char[] is forced by uvw
			// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
			auto req = std::make_unique<char[]>(sizeof(UDPTrackerAnnounceRequest));
			UDPTrackerRequestHeader header(response.connection_id(), 1);
			new(req.get()) UDPTrackerAnnounceRequest(header, sha1_raw, peer_id, 0, 431102370, 0, 0, 0, 0, numwant, 1);
			*last_transaction_id = header.transaction_id();
			udp.send(addr, std::move(req), sizeof(UDPTrackerAnnounceRequest));
			timer->again();

			*stage = AnnounceStage::announcing;
		} else {
			if(data.length < sizeof(UDPTrackerAnnounceResponse)) {
				return;
			}
			// It's fine, we only read UDPTrackerConnectResponse so no leaks are possible
			UDPTrackerAnnounceResponse response; // NOLINT(cppcoreguidelines-pro-type-member-init)
			memcpy(&response, data.data.get(), sizeof(response));
			if(response.header().transaction_id() != *last_transaction_id || response.header().action() != 1) {
				return;
			}

			udp.close();
			timer->close();

			if((data.length - sizeof(UDPTrackerAnnounceResponse)) % 6 != 0) {
				return;
			}
			std::vector<PeerAddress> peers;
			auto ptr = data.data.get();
			for(size_t i = sizeof(UDPTrackerAnnounceResponse); i < data.length; i += 6) {
				Blob slice(6, 0);
				// We have already checked that the data length is correct
				// NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
				std::copy(ptr + i, ptr + i + 6, slice.begin());
				peers.emplace_back(PeerAddressIPv4::from_bittorrent(slice));
			}
			callback(peers);
		}
	});

	udp->recv();


	timer->once<uvw::CloseEvent>([](const uvw::CloseEvent&, uvw::TimerHandle& timer) {
		timer.clear();
	});

	timer->once<uvw::TimerEvent>([udp](const uvw::TimerEvent&, uvw::TimerHandle& timer) {
		udp->close();
		timer.close();
	});

	timer->start(std::chrono::milliseconds{15000}, std::chrono::milliseconds{15000});
}