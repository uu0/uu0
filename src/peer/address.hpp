#ifndef PEER_ADDRESS_HPP
#define PEER_ADDRESS_HPP


#include "util/blob.hpp"
#include <msgpack.hpp>


class PeerAddressBase {
protected:
	[[nodiscard]] virtual bool is_less(const PeerAddressBase& other) const = 0;
	[[nodiscard]] virtual bool is_equal(const PeerAddressBase& other) const = 0;
	[[nodiscard]] virtual std::unique_ptr<PeerAddressBase> clone() const = 0;

public:
	virtual ~PeerAddressBase() = default;
	[[nodiscard]] virtual std::string to_string() const = 0;
	[[nodiscard]] virtual bool is_connectable() const = 0;
	virtual void decode_msgpack(Blob raw) = 0;
	virtual void visit(const std::function<void(sockaddr*)>& callback) const = 0;
	friend bool operator<(const PeerAddressBase& a, const PeerAddressBase& b);
	friend bool operator==(const PeerAddressBase& a, const PeerAddressBase& b);
	[[nodiscard]] virtual Blob to_msgpack() const = 0;

	template<typename Stream> msgpack::packer<Stream>& msgpack_pack(msgpack::packer<Stream>& packer) const {
		return packer.pack(to_msgpack());
	}

	msgpack::object const& msgpack_unpack(msgpack::object const& obj);

	friend class PeerAddress;
};


class PeerAddress {
	std::unique_ptr<PeerAddressBase> inner;

public:
	template<typename T> PeerAddress(T value): inner(std::make_unique<T>(value)) {
	}
	PeerAddress(const PeerAddress& other);
	PeerAddress(PeerAddress&& other) noexcept;
	~PeerAddress() = default;
	PeerAddress& operator=(const PeerAddress& other);
	PeerAddress& operator=(PeerAddress&& other) noexcept;

	[[nodiscard]] bool is_connectable() const;
	[[nodiscard]] std::string to_string() const;
	[[nodiscard]] bool operator<(const PeerAddress& other) const;
	[[nodiscard]] bool operator==(const PeerAddress& other) const;
	void visit(const std::function<void(sockaddr*)>& callback) const;
};


#endif