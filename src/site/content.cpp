#include "content.hpp"
#include "crypt/ecc.hpp"
#include "util/base64.hpp"
#include "util/blob.hpp"
#include <cassert>
#include <cstring>
#include <iostream>
#include <rapidjson/document.h>
#include <rapidjson/encodings.h>
#include <rapidjson/prettywriter.h>
// IWYU asks us to use different paths for the include below
#include <rapidjson/reader.h> // IWYU pragma: keep
// IWYU pragma: no_include "rapidjson/reader.h"
#include <rapidjson/stringbuffer.h>


bool verify_content(const std::string& content, std::vector<std::string> allowed_addresses, int min_valid_count) {
	if(strlen(content.c_str()) != content.size()) {
		// Zero byte
		return false;
	}
	rapidjson::Document doc;
	doc.Parse(content.c_str());
	if(doc.HasParseError()) {
		return false;
	}

	if(!doc.IsObject() || !doc.HasMember("signs") || !doc["signs"].IsObject()) {
		return false;
	}

	auto signs = std::move(doc["signs"]);
	doc.EraseMember("signs"); // preserves key order

	// Encode new content
	rapidjson::StringBuffer buffer;
	rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
	writer.SetIndent(' ', 0);
	doc.Accept(writer);
	std::string subject(buffer.GetString());
	// Fix spacing
	Blob new_subject;
	for(char c: subject) {
		if(c != '\n') {
			new_subject.push_back(c);
			continue;
		}
		// clang-tidy doesn't work well with assert
		assert(!new_subject.empty()); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
		if(new_subject.back() == ',') {
			new_subject.push_back(' ');
		}
	}

	// While we didn'task rapidjson to sort the keys, we can assume they are
	// already sorted at the moment of signing, and using EraseMember instead of
	// RemoveMember above guarantees that the order is retained
	Blob hash = CryptECC::double_format(new_subject);

	int valid_count = 0;
	for(auto it = signs.MemberBegin(); it != signs.MemberEnd(); ++it) {
		auto address = it->name.GetString();
		if(std::find(allowed_addresses.begin(), allowed_addresses.end(), address) == allowed_addresses.end()) {
			continue;
		}
		if(!it->value.IsString()) {
			continue;
		}
		try {
			auto signature = base64::decode(it->value.GetString());
			auto public_key = crypt_ecc.recover_public_key(signature, hash);
			if(public_key.to_address() != address) {
				continue;
			}
			valid_count++;
		} catch(std::invalid_argument& e) {
			std::cout << e.what() << std::endl;
		} catch(std::runtime_error& e) {
			std::cout << e.what() << std::endl;
		}
	}
	return valid_count >= min_valid_count;
}