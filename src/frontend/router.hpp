#ifndef FRONTEND_ROUTER_HPP
#define FRONTEND_ROUTER_HPP


#include "httprequest.hpp"


class FrontendRouter {
	HTTPRequest& http;

public:
	explicit FrontendRouter(HTTPRequest& http);
	void route();
};


#endif