#include "ecc.hpp"
#include "util/base58.hpp"
#include "util/hash.hpp"
#include <iostream>


using namespace std::literals::string_literals;


ECGroup::ECGroup(ECGroup&& other) noexcept {
	ec_group = other.ec_group;
	public_key_length = other.public_key_length;
	other.ec_group = nullptr;
}


ECGroup::ECGroup(const BigNumber& p, const BigNumber& a, const BigNumber& b) {
	public_key_length = (p.size() + 7) / 8;
	ec_group = EC_GROUP_new_curve_GFp(p.bn, a.bn, b.bn, BigNumberContext::get());
	if(ec_group == nullptr) {
		throw std::runtime_error("Could not create group object");
	}
}


ECGroup::~ECGroup() {
	EC_GROUP_free(ec_group);
}


ECGroup& ECGroup::operator=(ECGroup&& other) noexcept {
	EC_GROUP_free(ec_group);
	ec_group = other.ec_group;
	public_key_length = other.public_key_length;
	other.ec_group = nullptr;
	return *this;
}


void ECGroup::set_generator(const ECPoint& point, const BigNumber& n, const BigNumber& h) {
	EC_GROUP_set_generator(ec_group, point.ec_point, n.bn, h.bn);
}


ECPoint ECGroup::multuply_shamir(const BigNumber& coeff_g, const ECPoint& point, const BigNumber& coeff) const {
	ECPoint result(*this);
	if(EC_POINT_mul(ec_group, result.ec_point, coeff_g.bn, point.ec_point, coeff.bn, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not shamir-multiply ECPoint");
	}
	return result;
}


ECPoint::ECPoint(const ECGroup& group) {
	ec_point = EC_POINT_new(group.ec_group);
	if(ec_point == nullptr) {
		throw std::runtime_error("Could not allocate ECPoint");
	}
}


ECPoint::ECPoint(ECPoint&& other) noexcept {
	ec_point = other.ec_point;
	other.ec_point = nullptr;
}


ECPoint::ECPoint(const BigNumber& x, const BigNumber& y, const ECGroup& group): ECPoint(group) {
	if(EC_POINT_set_affine_coordinates_GFp(group.ec_group, ec_point, x.bn, y.bn, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not set point coordinates");
	}
}


ECPoint::ECPoint(const Blob& blob, const ECGroup& group): ECPoint(group) {
	if(EC_POINT_oct2point(group.ec_group, ec_point, blob.data(), blob.size(), BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not recover point from compressed form");
	}
}


ECPoint::~ECPoint() {
	EC_POINT_free(ec_point);
}


ECPoint& ECPoint::operator=(ECPoint&& other) noexcept {
	EC_POINT_free(ec_point);
	ec_point = other.ec_point;
	other.ec_point = nullptr;
	return *this;
}


BigNumber ECPoint::get_x(const ECGroup& group) const {
	BigNumber res;
	if(EC_POINT_get_affine_coordinates_GFp(group.ec_group, ec_point, res.bn, nullptr, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not get X coordinate of ECPoint");
	}
	return res;
}
BigNumber ECPoint::get_y(const ECGroup& group) const {
	BigNumber res;
	if(EC_POINT_get_affine_coordinates_GFp(group.ec_group, ec_point, nullptr, res.bn, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not get Y coordinate of ECPoint");
	}
	return res;
}

void ECPoint::set_coords(const BigNumber& x, const BigNumber& y, const ECGroup& group) {
	if(EC_POINT_set_affine_coordinates_GFp(group.ec_group, ec_point, x.bn, y.bn, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Failed to update R coordinates");
	}
}


std::string ECPoint::to_string(const ECGroup& group) const {
	return get_x(group).to_string() + ", " + get_y(group).to_string();
}


ECPublicKey::ECPublicKey(const ECGroup& group, ECPoint&& point, bool is_compressed): group(group), point(std::move(point)), is_compressed(is_compressed) {
}


std::string ECPublicKey::to_address() const {
	Blob compressed;
	if(is_compressed) {
		unsigned char id = (2 + static_cast<BN_ULONG>(point.get_y(group) % 0x02_bn));
		compressed = Blob(1, id) + point.get_x(group).bytes(group.public_key_length);
	} else {
		compressed = "\x04"_blob;
		compressed += point.get_x(group).bytes(group.public_key_length);
		compressed += point.get_y(group).bytes(group.public_key_length);
	}

	auto sha256_hashed = sha256(compressed.data(), compressed.size());
	auto ripemd160_hashed = ripemd160(sha256_hashed.data(), sha256_hashed.size());

	auto blob = "\x00"_blob + Blob(ripemd160_hashed.data(), ripemd160_hashed.size());
	return base58::encode_check(blob);
}


std::string ECPublicKey::to_string() const {
	if(is_compressed) {
		return "<ECPublicKey compressed " + point.to_string(group) + ">";
	} else {
		return "<ECPublicKey uncompressed " + point.to_string(group) + ">";
	}
}


CryptECC::CryptECC() {
	// secp256k1
	p = 0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f_bn;
	auto a = 0x00_bn;
	auto b = 0x07_bn;
	group = ECGroup(p, a, b);

	n = 0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141_bn;
	auto gx = 0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798_bn;
	auto gy = 0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8_bn;
	auto h = 0x01_bn;
	group.set_generator(ECPoint{gx, gy, group}, n, h);

	std::cout << "CryptECC created!!" << std::endl;
}


ECPublicKey CryptECC::recover_public_key(const Blob& signature, const Blob& hash) {
	// Sanity check: is this signature recoverable?
	if(signature.size() != 1 + 2 * group.public_key_length) {
		throw std::invalid_argument("Wrong signature size");
	}

	int recid = (signature[0] - 27) % 4;
	BigNumber r(signature.substr(1, group.public_key_length));
	BigNumber s(signature.substr(group.public_key_length + 1));

	// Verify bounds
	if(r >= n) {
		throw std::invalid_argument("r is out of bounds");
	}
	if(s >= n) {
		throw std::invalid_argument("s is out of bounds");
	}

	BigNumber z(hash.substr(0, (n.size() + 7) / 8));
	auto rinv = r.inverse(n);
	auto u1 = (-z * rinv) % n;
	auto u2 = (s * rinv) % n;

	// Recover R
	auto rx = r + BigNumber(recid / 2) * n;
	if(rx >= p) {
		throw std::invalid_argument("Rx is out of bounds");
	}
	ECPoint rp("\x02"_blob + rx.bytes(group.public_key_length), group);
	auto ry = rp.get_y(group);
	if(static_cast<BN_ULONG>(ry % 0x02_bn) != recid % 2) {
		// Fix Ry sign
		ry = p - ry;
		rp.set_coords(rx, ry, group);
	}

	// Recover public key
	return ECPublicKey{group, group.multuply_shamir(u1, rp, u2), signature[0] >= 31};
}


Blob CryptECC::double_format(const Blob& subject) {
	Blob size_encoded;
	auto n = subject.size();
	if(n < 253) {
		size_encoded.push_back(n);
	} else if(n < 0x10000) {
		size_encoded.push_back('\xfd');
		size_encoded.push_back(n & 255U);
		size_encoded.push_back((n >> 8U) & 255U);
	} else if(n < 0x100000000) {
		size_encoded.push_back('\xfe');
		size_encoded.push_back(n & 255U);
		size_encoded.push_back((n >> 8U) & 255U);
		size_encoded.push_back((n >> 16U) & 255U);
		size_encoded.push_back((n >> 24U) & 255U);
	} else {
		size_encoded.push_back('\xff');
		size_encoded.push_back(n & 255U);
		size_encoded.push_back((n >> 8U) & 255U);
		size_encoded.push_back((n >> 16U) & 255U);
		size_encoded.push_back((n >> 24U) & 255U);
		size_encoded.push_back((n >> 32U) & 255U);
		size_encoded.push_back((n >> 40U) & 255U);
		size_encoded.push_back((n >> 48U) & 255U);
		size_encoded.push_back((n >> 56U) & 255U);
	}

	auto encoded = "\x18" "Bitcoin Signed Message:\n"_blob + size_encoded + subject;

	auto sha256_1 = sha256(encoded.data(), encoded.size());
	auto sha256_2 = sha256(sha256_1.data(), sha256_1.size());
	return Blob(sha256_2.data(), sha256_2.size());
}


CryptECC crypt_ecc;