// Forced by rapidjson
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define RAPIDJSON_HAS_STDSTRING 1
#include "site/manager.hpp"
#include "site/site.hpp"
#include "util/json.hpp"
#include "websockethandler.hpp"
#include "websocketrequest.hpp"
#include <cstring>
#include <iostream>
#include <map>
#include <rapidjson/document.h>
#include <rapidjson/reader.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>


std::map<std::string, std::function<void(WebSocketHandler*, const rapidjson::Value&)>> WebSocketHandler::action_mapping = {
	{"fileNeed", &WebSocketHandler::action_file_need}
};


WebSocketHandler::WebSocketHandler(WebSocketRequest& request): request(request) {
	if(request.http.headers.count("Origin") == 0) {
		send_error("'Origin' header missing");
		request.http.close();
		return;
	}
	auto origin = request.http.headers.at("Origin");
	auto idx = origin.find("://");
	if(idx != std::string::npos) {
		origin = origin.substr(idx + 3);
	}
	idx = origin.find('/');
	if(idx != std::string::npos) {
		origin = origin.substr(0, idx);
	}
	if(origin.size() >= 8 && origin.substr(origin.size() - 8) == ".zeronet") {
		origin = origin.substr(0, origin.size() - 8);
	}
	site_address = origin;
	if(!Site::is_valid_address(site_address)) {
		send_error("Invalid site address");
		request.http.close();
		return;
	}

	site = site_manager.add(site_address);
	auto& emitter = site.lock()->emitter;
	emitter.clear();
	site_file_done_event = emitter.on<SiteFileDoneEvent>([this](const SiteFileDoneEvent& event, SiteEmitter&) {
		auto info = get_site_info();
		info["event"] = JSONArray{"file_done", event.path};
		send_cmd("setSiteInfo", std::move(info));
	});
}


WebSocketHandler::~WebSocketHandler() {
	if(!site.expired()) {
		auto& emitter = site.lock()->emitter;
		if(site_file_done_event) {
			emitter.erase(*site_file_done_event);
		}
	}
}


void WebSocketHandler::handle(const std::string& message) {
	if(strlen(message.c_str()) != message.size()) {
		// Zero byte
		send_error("Zero byte in message");
		return;
	}

	rapidjson::Document doc;
	doc.Parse(message.c_str());
	if(doc.HasParseError()) {
		send_error("Could not parse message as JSON");
		return;
	}
	if(!doc.IsObject()) {
		send_error("Message is not an object");
		return;
	}
	if(!doc.HasMember("cmd")) {
		send_error("Message does not have 'cmd' attribute");
		return;
	}
	if(!doc["cmd"].IsString()) {
		send_error("'cmd' attribute is not a string");
		return;
	}
	if(!doc.HasMember("params")) {
		send_error("Message does not have 'params' attribute");
		return;
	}
	if(!doc["params"].IsArray() && !doc["params"].IsObject()) {
		send_error("'params' attribute is not an array and not an object");
		return;
	}

	std::string cmd = doc["cmd"].GetString();
	if(action_mapping.count(cmd) == 0) {
		send_error("Unknown command '" + cmd + "'");
		return;
	}

	try {
		action_mapping[cmd](this, doc["params"]);
	} catch(std::invalid_argument& e) {
		send_error(e.what());
	}
}


void WebSocketHandler::send_error(const std::string& text) {
	request.send(stringify_json(JSONObject{
		{"error", text}
	}));
}


void WebSocketHandler::send_cmd(const std::string& cmd, JSONObject&& params) {
	request.send(stringify_json(JSONObject{
		{"cmd", cmd},
		{"params", params}
	}));
}


JSONObject WebSocketHandler::get_site_info() const {
	if(site.expired()) {
		return JSONObject{};
	}
	auto ptr = site.lock();
	auto address = ptr->get_case_sensitive_address();
	return JSONObject{
		// Random number (actually pi in hexadimical form)
		{"auth_key", "03243f6a8885a308d313198a2e03707344a4093822299f31d0082efa98ec4e6c"},
		{"auth_address", "1NoAddressAvailableYet"},
		{"cert_user_id", json_null},
		{"address", address},
		{"address_short", address.substr(0, 6) + "..." + address.substr(address.size() - 4)},
		{"settings", JSONObject{}},
		{"content_updated", json_null},
		{"bad_files", 0},
		{"size_limit", 20 * 1024 * 1024}, // 20 MiB
		{"next_size_limit", 50 * 1024 * 1024}, // 50 MiB
		{"peers", 0},
		{"started_task_num", 0},
		{"tasks", 0},
		{"workers", 0},
		{"content", JSONObject{}}
	};
}


void WebSocketHandler::action_file_need(const rapidjson::Value& params) {
	if(site.expired()) {
		send_error("Site is deleted");
		return;
	}
	auto site_ptr = site.lock();
	auto [inner_path, timeout, priority] = get_params<std::string, int, int>(params, "inner_path", Default{"timeout", 300}, Default{"priority", 6});
	site_ptr->update_content(site_ptr->get_content_of(inner_path));
}