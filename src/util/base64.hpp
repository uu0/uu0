#ifndef UTIL_BASE64_HPP
#define UTIL_BASE64_HPP


#include "blob.hpp"


namespace base64 {
	Blob decode(const std::string& code);
	std::string encode(const Blob& data);
};


#endif