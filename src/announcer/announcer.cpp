#include "announcer.hpp"
#include "http.hpp"
#include "udp.hpp"
#include "util/hash.hpp"
#include "util/rand.hpp"
#include <random>


Announcer announcer;


// peer_id is initialized by constructor body
Announcer::Announcer() { // NOLINT(cppcoreguidelines-pro-type-member-init)
	std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	std::uniform_int_distribution<size_t> rnd(0, alphabet.size() - 1);

	std::string peer_id_str = "-UU0-00001-";
	while(peer_id_str.size() < peer_id.size()) {
		peer_id_str += alphabet[rnd(random_source::gen)];
	}
	std::copy(peer_id_str.begin(), peer_id_str.end(), peer_id.begin());
}


void Announcer::add_tracker(const std::string& tracker) {
	trackers.push_back(tracker);
}


void Announcer::announce(const std::string& subject, int numwant, const std::string& tracker, const std::function<void(std::vector<PeerAddress>)>& callback) {
	auto loop = uvw::Loop::getDefault();

	auto protocol = tracker.substr(0, tracker.find("://")); // http
	auto hostpath = tracker.substr(tracker.find("://") + 3); // example.com:80/announce
	if(hostpath.find('/') == std::string::npos) {
		hostpath += "/";
	}

	auto host = hostpath.substr(0, hostpath.find('/')); // example.com:80
	std::string hostname;
	std::string port;
	if(host.find(':') == std::string::npos) {
		hostname = host;
		port = "80";
	} else {
		hostname = host.substr(0, host.find(':'));
		port = host.substr(host.find(':') + 1);
	}

	auto dns = loop->resource<uvw::GetAddrInfoReq>();

	dns->once<uvw::AddrInfoEvent>([=](const uvw::AddrInfoEvent& info, uvw::GetAddrInfoReq&) {
		auto sha1_raw = sha1(subject.data(), subject.size());

		std::optional<std::function<void(std::array<unsigned char, 20>, int, std::array<char, 20>, sockaddr, std::string, std::string, std::string, std::function<void(std::vector<PeerAddress>)>)>> handler;
		if(protocol == "http") {
			handler = announce_http;
		} else if(protocol == "udp") {
			handler = announce_udp;
		} else {
			return;
		}
		handler.value()(sha1_raw, numwant, peer_id, *info.data->ai_addr, hostname, port, hostpath, callback);
	});

	dns->addrInfo(hostname, port);
}