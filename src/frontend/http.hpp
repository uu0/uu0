#ifndef FRONTEND_HTTP_HPP
#define FRONTEND_HTTP_HPP


#include "httprequest.hpp"
#include "websockethandler.hpp"
#include "websocketrequest.hpp"
#include <map>
#include <websocket-parser/websocket_parser.h>


class HTTPServer {
public:
	void listen(const std::string& ip, int port);
};


struct WebSocketClientState {
	WebSocketRequest request;
	websocket_parser_settings settings{};
	websocket_parser parser{};
	std::vector<std::string> frame_bodies;
	websocket_flags frame_op{};
	std::optional<WebSocketHandler> handler;

	explicit WebSocketClientState(HTTPRequest& http);
};


struct HTTPClientState {
	std::shared_ptr<uvw::TCPHandle> tcp;
	HTTPRequest request;
	enum class HeaderState {
		none,
		field,
		value
	} header_state = HeaderState::none;
	std::string last_header_field{};
	std::string last_header_value{};
	std::unique_ptr<WebSocketClientState> ws{};
};


extern HTTPServer http_server;


#endif