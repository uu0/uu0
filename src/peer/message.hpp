#ifndef PEER_MESSAGE_HPP
#define PEER_MESSAGE_HPP


#include "protocols/ipv4.hpp"
#include "protocols/ipv6.hpp"
#include "protocols/onion.hpp"
#include "util/blob.hpp"
#include <map>
#include <msgpack.hpp>


struct ConnectionAbstractMessage {
	std::string cmd;
	int req_id;
	MSGPACK_DEFINE_MAP(cmd, req_id);
};


struct ConnectionMessageResponse {
	std::string cmd;
	int req_id;
	std::optional<int> to;
	MSGPACK_DEFINE_MAP(cmd, req_id, to);
};


struct ConnectionHandshake {
	std::string version;
	std::string protocol;
	std::optional<bool> use_bin_type;
	std::string peer_id;
	int fileserver_port;
	std::optional<bool> port_opened;
	std::string target_ip;
	int rev;
	std::vector<std::string> crypt_supported;
	std::optional<std::string> crypt;
	std::optional<int> time;
	std::optional<std::string> onion;
	std::optional<bool> tracker_connection;
	MSGPACK_DEFINE_MAP(
		version, protocol, use_bin_type, peer_id, fileserver_port, port_opened,
		target_ip, rev, crypt_supported, crypt, time, onion, tracker_connection
	);
};

struct ConnectionMessageHandshake: public ConnectionAbstractMessage {
	ConnectionHandshake params;
	MSGPACK_DEFINE_MAP(cmd, req_id, params);
};


struct ConnectionPex {
	std::string site;
	std::vector<PeerAddressIPv4> peers;
	std::vector<PeerAddressIPv6> peers_ipv6;
	std::vector<PeerAddressOnion> peers_onion;
	int need;
	MSGPACK_DEFINE_MAP(site, peers, peers_ipv6, peers_onion, need);
};

struct ConnectionMessagePex: public ConnectionAbstractMessage {
	ConnectionPex params;
	MSGPACK_DEFINE_MAP(cmd, req_id, params);
};

struct ConnectionMessagePexResponse: public ConnectionMessageResponse {
	std::vector<PeerAddressIPv4> peers;
	std::vector<PeerAddressIPv6> peers_ipv6;
	std::vector<PeerAddressOnion> peers_onion;
	MSGPACK_DEFINE_MAP(cmd, req_id, to, peers, peers_ipv6, peers_onion);
};


struct ConnectionListModified {
	std::string site;
	int since;
	MSGPACK_DEFINE_MAP(site, since);
};

struct ConnectionMessageListModified: public ConnectionAbstractMessage {
	ConnectionListModified params;
	MSGPACK_DEFINE_MAP(cmd, req_id, params);
};

struct ConnectionMessageListModifiedResponse: public ConnectionMessageResponse {
	std::map<std::string, int> modified_files;
	MSGPACK_DEFINE_MAP(cmd, req_id, to, modified_files);
};


struct ConnectionReadFile {
	std::string site;
	std::string inner_path;
	size_t location;
	size_t read_bytes;
	std::optional<size_t> file_size;
	MSGPACK_DEFINE_MAP(site, inner_path, location, read_bytes, file_size);
};

struct ConnectionMessageReadFile: public ConnectionAbstractMessage {
	ConnectionReadFile params;
	MSGPACK_DEFINE_MAP(cmd, req_id, params);
};

struct ConnectionMessageReadFileResponse: public ConnectionMessageResponse {
	std::optional<Blob> body;
	MSGPACK_DEFINE_MAP(cmd, req_id, to, body);
};


#endif