#ifndef PROTOCOLS_IPV4_HPP
#define PROTOCOLS_IPV4_HPP


#include "peer/address.hpp"
#include "util/blob.hpp"
#include <byteswap.h>


class PeerAddressIPv4: public PeerAddressBase {
protected:
	[[nodiscard]] bool is_less(const PeerAddressBase& other) const override {
		auto other_cast = dynamic_cast<const PeerAddressIPv4&>(other);
		if(ip.s_addr != other_cast.ip.s_addr) {
			return ntohl(ip.s_addr) < ntohl(other_cast.ip.s_addr);
		}
		return ntohs(port) < ntohs(other_cast.port);
	}


	[[nodiscard]] bool is_equal(const PeerAddressBase& other) const override {
		auto other_cast = dynamic_cast<const PeerAddressIPv4&>(other);
		return ntohl(ip.s_addr) == ntohl(other_cast.ip.s_addr) && ntohs(port) == ntohs(other_cast.port);
	}


	[[nodiscard]] std::unique_ptr<PeerAddressBase> clone() const override {
		return std::make_unique<PeerAddressIPv4>(*this);
	}


public:
	in_addr ip = {};
	in_port_t port = {};


	PeerAddressIPv4() = default;


	PeerAddressIPv4(in_addr ip, in_port_t port): ip(ip), port(port) {
	}


	[[nodiscard]] bool is_connectable() const override {
		return ntohs(port) != 0 && ntohs(port) != 1;
	}


	static PeerAddressIPv4 from_bittorrent(Blob raw) {
		if(raw.size() != 6) {
			throw std::length_error("Invalid PeerAddress length");
		}
		std::array<uint64_t, 6> raw_unsigned{};
		std::copy(raw.begin(), raw.end(), raw_unsigned.begin());
		PeerAddressIPv4 addr{};
		addr.ip.s_addr = htonl(raw_unsigned[3] | (raw_unsigned[2] << 8U) | (raw_unsigned[1] << 16U) | (raw_unsigned[0] << 24U));
		addr.port = htons(raw_unsigned[5] | (raw_unsigned[4] << 8U));
		return addr;
	}


	void decode_msgpack(Blob raw) override {
		*this = from_bittorrent(raw);
		port = bswap_16(port);
	}


	[[nodiscard]] Blob to_msgpack() const override {
		auto native_ip = ntohl(ip.s_addr);
		auto native_port = ntohs(port);
		return {
			static_cast<unsigned char>(native_ip >> 24U),
			static_cast<unsigned char>((native_ip >> 16U) & 0xffU),
			static_cast<unsigned char>((native_ip >> 8U) & 0xffU),
			static_cast<unsigned char>(native_ip & 0xffU),
			static_cast<unsigned char>(native_port & 0xffU),
			static_cast<unsigned char>((native_port >> 8U) & 0xffU) // NOLINT(hicpp-signed-bitwise)
		};
	}


	[[nodiscard]] std::string to_string() const override {
		std::array<char, INET_ADDRSTRLEN> buffer{};
		inet_ntop(AF_INET, &ip, buffer.data(), buffer.size());
		return std::string(buffer.data()) + ":" + std::to_string(ntohs(port));
	}


	void visit(const std::function<void(sockaddr*)>& callback) const override {
		sockaddr_in addr{AF_INET, port, ip, {}};
		// sockaddr is forced by socket API
		callback(reinterpret_cast<sockaddr*>(&addr)); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
	}
};


#endif