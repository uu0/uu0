#include "bignumber.hpp"
#include "blob.hpp"
#include <cstring>


thread_local BigNumberContext BigNumberContext::ctx;


BigNumberContext::BigNumberContext() {
	bn_ctx = BN_CTX_new();
	if(bn_ctx == nullptr) {
		throw std::runtime_error("Could not allocate BN_CTX");
	}
}


BigNumberContext::~BigNumberContext() {
	BN_CTX_free(bn_ctx);
}


BN_CTX* BigNumberContext::get() {
	return ctx.bn_ctx;
}


BigNumber::BigNumber() {
	bn = BN_new();
	if(bn == nullptr) {
		throw std::runtime_error("Could not create BigNumber");
	}
}


BigNumber::BigNumber(BIGNUM* bn): bn(bn) {
}


BigNumber::BigNumber(const Blob& blob): BigNumber() {
	// reinterpret_cast is forced by OpenSSL conventions
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
	if(BN_bin2bn(reinterpret_cast<const unsigned char*>(blob.data()), blob.size(), bn) == nullptr) {
		throw std::runtime_error("Could not initialize BigNumber from Blob");
	}
}


BigNumber::BigNumber(BN_ULONG value): BigNumber() {
	if(BN_set_word(bn, value) == 0) {
		throw std::runtime_error("Could not initialize BigNumber from value");
	}
}


BigNumber::BigNumber(const BigNumber& other): BigNumber() {
	if(BN_copy(bn, other.bn) == nullptr) {
		throw std::runtime_error("Could not copy-construct BigNumber");
	}
}


BigNumber::BigNumber(BigNumber&& other) noexcept {
	bn = other.bn;
	other.bn = nullptr;
}


BigNumber::~BigNumber() {
	BN_free(bn);
}


BigNumber& BigNumber::operator=(const BigNumber& other) {
	if(this == &other) {
		return *this;
	}
	if(BN_copy(bn, other.bn) == nullptr) {
		throw std::runtime_error("Could not copy-assign BigNumber");
	}
	return *this;
}


BigNumber& BigNumber::operator=(BigNumber&& other) noexcept {
	BN_free(bn);
	bn = other.bn;
	other.bn = nullptr;
	return *this;
}


bool BigNumber::operator==(const BigNumber& other) const {
	return BN_cmp(bn, other.bn) == 0;
}
bool BigNumber::operator!=(const BigNumber& other) const {
	return BN_cmp(bn, other.bn) != 0;
}
bool BigNumber::operator<(const BigNumber& other) const {
	return BN_cmp(bn, other.bn) < 0;
}
bool BigNumber::operator>(const BigNumber& other) const {
	return BN_cmp(bn, other.bn) > 0;
}
bool BigNumber::operator<=(const BigNumber& other) const {
	return BN_cmp(bn, other.bn) <= 0;
}
bool BigNumber::operator>=(const BigNumber& other) const {
	return BN_cmp(bn, other.bn) >= 0;
}


BigNumber BigNumber::operator+(const BigNumber& other) const {
	BigNumber result;
	if(BN_add(result.bn, bn, other.bn) == 0) {
		throw std::runtime_error("Could not sum up two BigNumbers");
	}
	return result;
}
BigNumber BigNumber::operator-(const BigNumber& other) const {
	BigNumber result;
	if(BN_sub(result.bn, bn, other.bn) == 0) {
		throw std::runtime_error("Could not subtract two BigNumbers");
	}
	return result;
}
BigNumber BigNumber::operator*(const BigNumber& other) const {
	BigNumber result;
	if(BN_mul(result.bn, bn, other.bn, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not multiply two BigNumbers");
	}
	return result;
}
BigNumber BigNumber::operator/(const BigNumber& other) const {
	BigNumber result;
	if(BN_div(result.bn, nullptr, bn, other.bn, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not divide two BigNumbers");
	}
	return result;
}
BigNumber BigNumber::operator%(const BigNumber& other) const {
	BigNumber result;
	if(BN_div(nullptr, result.bn, bn, other.bn, BigNumberContext::get()) == 0) {
		throw std::runtime_error("Could not divide two BigNumbers");
	}
	return result;
}


BigNumber BigNumber::inverse(const BigNumber& modulo) const {
	BigNumber result;
	if(BN_mod_inverse(result.bn, bn, modulo.bn, BigNumberContext::get()) == nullptr) {
		throw std::invalid_argument("Could not compute inverse modulo given argument");
	}
	return result;
}


BigNumber BigNumber::operator-() const {
	return 0x00_bn - *this;
}


BigNumber::operator BN_ULONG() const {
	return BN_get_word(bn);
}


Blob BigNumber::bytes(size_t sz) const {
	Blob blob(BN_num_bytes(bn), '\0');
	// reinterpret_cast is forced by OpenSSL conventions
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
	if(BN_bn2bin(bn, reinterpret_cast<unsigned char*>(blob.data())) == 0) {
		throw std::runtime_error("Could not convert BigNumber to Blob");
	}
	if(blob.size() > sz) {
		throw std::invalid_argument("Blob is too large to fit into given buffer");
	}
	return Blob(sz - blob.size(), '\0') + blob;
}


size_t BigNumber::size() const {
	return bn == nullptr ? 0 : BN_num_bits(bn);
}


std::string BigNumber::to_string() const {
	if(BN_is_zero(bn)) { // NOLINT(readability-implicit-bool-conversion)
		return "0x00";
	} else {
		auto alphabet = "0123456789abcdef";
		auto blob = bytes(BN_num_bytes(bn));
		std::string result = "0x";
		for(unsigned char x: blob) {
			// It's obvious no array overrun is possible. Bitwise problem is a false positive
			result.push_back(alphabet[(x >> 4U) & 15U]); // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic, hicpp-signed-bitwise)
			result.push_back(alphabet[x & 15U]); // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
		}
		return result;
	}
}


BigNumber operator"" _bn(const char* c_str) {
	std::string str(c_str);
	if(str.size() < 2 || str[0] != '0' || str[1] != 'x') {
		throw std::invalid_argument("_bn can only decode hexadimical literals");
	}
	if(str[2] == '\0') {
		throw std::invalid_argument("Empty _bn literals are not allowed");
	}
	if(str.size() % 2 == 1) {
		throw std::invalid_argument("_bn only accepts an even count of literals");
	}
	std::string alphabet = "0123456789abcdef";
	Blob data;
	for(size_t i = 2; i < str.size(); i += 2) {
		size_t a = find(alphabet.begin(), alphabet.end(), str[i]) - alphabet.begin();
		size_t b = find(alphabet.begin(), alphabet.end(), str[i + 1]) - alphabet.begin();
		if(a == 16 || b == 16) {
			throw std::invalid_argument("Unknown hexadimical character (make sure you're using lowercase)");
		}
		data.push_back((a << 4U) | b);
	}
	// reinterpret_cast is forced by OpenSSL conventions
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
	return BigNumber{BN_bin2bn(reinterpret_cast<unsigned char*>(data.data()), data.size(), nullptr)};
}