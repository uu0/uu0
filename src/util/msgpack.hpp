#ifndef UTIL_MSGPACK_HPP
#define UTIL_MSGPACK_HPP


#include <msgpack/v2/object_fwd.hpp>


void fix_msgpack_object(msgpack::object& obj);


#endif