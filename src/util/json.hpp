#ifndef UTIL_JSON_HPP
#define UTIL_JSON_HPP


#include <map>
#include <rapidjson/document.h>
#include <string>
#include <variant>
#include <vector>


struct JSONObject;
struct JSONArray;
struct JSONString;
struct JSONInteger;
struct JSONNull;

using JSON = std::variant<JSONObject, JSONArray, JSONString, JSONInteger, JSONNull>;


struct JSONObject {
	std::map<std::string, JSON> attributes;
	JSONObject() = default;
	JSONObject(std::initializer_list<std::pair<const std::string, JSON>> attributes);
	JSON& operator[](const std::string& attr);
};

struct JSONArray {
	std::vector<JSON> list;
	JSONArray() = default;
	JSONArray(std::initializer_list<JSON> list);
};

struct JSONString {
	std::string str;
	JSONString(std::string str); // NOLINT(hicpp-explicit-conversions)
	JSONString(const char* str); // NOLINT(hicpp-explicit-conversions)
};

struct JSONInteger {
	int64_t integer;
	JSONInteger(int64_t integer); // NOLINT(hicpp-explicit-conversions)
};

struct JSONNull {
};


rapidjson::Value _encode_json_impl(JSONObject&& object, rapidjson::Document& doc);
rapidjson::Value _encode_json_impl(JSONArray&& array, rapidjson::Document& doc);
rapidjson::Value _encode_json_impl(JSONString&& string, rapidjson::Document& doc);
rapidjson::Value _encode_json_impl(JSONInteger&& integer, rapidjson::Document& doc);
rapidjson::Value _encode_json_impl(JSONNull&& null, rapidjson::Document& doc);

template<typename T> rapidjson::Value encode_json(T&& json, rapidjson::Document& doc) {
	return _encode_json_impl(std::forward<T>(json), doc);
}
template<> rapidjson::Value encode_json<JSON>(JSON&& json, rapidjson::Document& doc);

std::string stringify_json(JSON&& json);


static JSONNull json_null;


#endif