#include "announcer/announcer.hpp"
#include "content.hpp"
// IWYU asks us to use different path for the include below
#include "peer/address.hpp" // IWYU pragma: keep
#include "peer/peer.hpp"
#include "site.hpp"
#include "util/blob.hpp"
#include "util/tempfile.hpp"
#include <cassert>
#include <iostream>
#include <openssl/sha.h>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
// IWYU asks us to use different path for the include below
#include <rapidjson/reader.h> // IWYU pragma: keep
// IWYU pragma: no_include "rapidjson/reader.h"
#include <set>


SiteEmitter::SiteEmitter(Site& site): site(site) {
}


Site::Site(std::string address): emitter(*this), address(std::move(address)) {
	assert(is_valid_address(this->address)); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
}


bool Site::is_valid_address(const std::string& address) {
	if(address.size() < 32 || address.size() > 50 || address[0] != '0') {
		return false;
	}

	// TODO(ivanq): verify checksum

	auto case_encoded = address.substr(address.size() - 7);
	std::string alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
	uint64_t case_mapping = 0;
	uint64_t coeff = 1;
	for(int i = 0; i < 7; i++) {
		case_mapping += alphabet.find(case_encoded[i]) * coeff;
		coeff *= 36;
	}
	if(case_mapping >= (uint64_t{1} << (address.size() - 7))) {
		return false;
	}
	for(unsigned int i = 0; i < address.size() - 7; i++) {
		if(((case_mapping >> i) & 1U) == 0 && std::toupper(address[i]) == address[i]) {
			return false;
		}
	}
	return true;
}


bool Site::is_valid_case_sensitive_address(const std::string& address) {
	if(address.size() < 25 || address.size() > 43 || address[0] != '1') {
		return false;
	}

	// TODO(ivanq): verify checksum

	std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	return std::all_of(address.begin(), address.end(), [&alphabet](char c) {
		return alphabet.find(c) != std::string::npos;
	});
}


std::string Site::case_sensitive_address_to_address(std::string address) {
	assert(is_valid_case_sensitive_address(address)); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)

	uint64_t case_mapping = 0;
	for(unsigned int i = 0; i < address.size(); i++) {
		if(std::toupper(address[i]) == address[i]) {
			case_mapping |= uint64_t{1} << i;
		}
	}

	std::string alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
	std::string case_encoded;
	for(int i = 0; i < 7; i++) {
		case_encoded.push_back(alphabet[case_mapping % 36]);
		case_mapping /= 36;
	}

	std::transform(address.begin(), address.end(), address.begin(), [](unsigned char c) {
		return std::tolower(c);
	});

	return "0" + address.substr(1) + case_encoded;
}


std::string Site::get_case_sensitive_address() const {
	assert(is_valid_address(address)); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)

	auto case_encoded = address.substr(address.size() - 7);
	std::string alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
	uint64_t case_mapping = 0;
	uint64_t coeff = 1;
	for(int i = 0; i < 7; i++) {
		case_mapping += alphabet.find(case_encoded[i]) * coeff;
		coeff *= 36;
	}

	std::string result = "1";
	for(unsigned int i = 1; i < address.size() - 7; i++) {
		if(((case_mapping >> i) & 1U) == 1) {
			result += std::toupper(address[i]);
		} else {
			result += address[i];
		}
	}
	return result;
}


void Site::add_peers(const std::vector<PeerAddress>& new_peers, const std::string& origin) {
	auto this_ptr = std::weak_ptr(shared_from_this());

	for(auto& addr: new_peers) {
		if(peers.count(addr) != 0) {
			continue;
		}

		peers.emplace(addr, std::make_shared<Peer>(addr, origin));

		auto peer = peers.at(addr);

		peer->emitter.on<PeerConnectingEvent>([this_ptr, addr](const PeerConnectingEvent&, PeerEmitter&) {
			if(!this_ptr.expired()) {
				this_ptr.lock()->idle_connectable_peers.erase(addr);
			}
		});

		peer->emitter.on<PeerConnectedEvent>([this_ptr, addr](const PeerConnectedEvent&, PeerEmitter&) {
			if(this_ptr.expired()) {
				return;
			}
			auto ptr = this_ptr.lock();
			ptr->release_worker(addr);

			auto peer = ptr->peers.at(addr);
			peer->pex(this_ptr, [this_ptr](const std::vector<PeerAddress>& peers) {
				if(!this_ptr.expired()) {
					this_ptr.lock()->add_peers(peers, "pex");
				}
			});
		});

		peer->emitter.on<PeerDisconnectedEvent>([this_ptr, addr](const PeerDisconnectedEvent&, PeerEmitter&) {
			if(this_ptr.expired()) {
				return;
			}
			auto ptr = this_ptr.lock();
			ptr->free_peers.erase(addr);
			ptr->idle_connectable_peers.insert(addr);
		});

		if(peer->is_connectable()) {
			idle_connectable_peers.insert(addr);
			peer->connect();
		}
	}
}


void Site::check_peers() {
	if(free_peers.size() < 30) {
		// Try connecting to some peers
		int cnt_connecting = 0;
		// Avoid changing a list we're iterating
		auto peer_copy = idle_connectable_peers;
		for(auto& addr: peer_copy) {
			peers[addr]->connect();
			cnt_connecting++;
		}
		if(cnt_connecting < 30) {
			// Get new peers from tracker
			for(const auto& tracker: announcer.trackers) {
				if(trackers_requesting.count(tracker) == 0) {
					trackers_requesting.insert(tracker);
					check_peers_tracker(tracker);
				}
			}
		}
	}
}


void Site::check_peers_tracker(const std::string& tracker, int tries, int numwant) {
	if(tries == 0) {
		trackers_requesting.erase(tracker);
		return;
	}

	auto this_ptr = std::weak_ptr(shared_from_this());
	announcer.announce(get_case_sensitive_address(), numwant, tracker, [=](auto new_peers) {
		if(this_ptr.expired()) {
			return;
		}
		auto ptr = this_ptr.lock();
		ptr->add_peers(new_peers, tracker);
		ptr->check_peers_tracker(tracker, tries - 1, numwant * 2);
	});
}


void Site::update() {
	while(updating_peer_count < 10) {
		updating_peer_count++;

		auto this_ptr = std::weak_ptr(shared_from_this());
		with_worker(
			100, "site update",
			&Peer::list_modified, this_ptr, 0,
			[this_ptr](const std::map<std::string, int>& contents) {
				if(this_ptr.expired()) {
					return;
				}

				auto ptr = this_ptr.lock();
				ptr->updating_peer_count--;
				std::vector< std::pair<std::string, int> > content_list(contents.size());
				std::copy(contents.begin(), contents.end(), content_list.begin());
				sort(content_list.begin(), content_list.end(), [](auto a, auto b) {
					return a.first.size() < b.first.size();
				});
				for(auto [path, time_modified]: content_list) {
					ptr->update_content(path, time_modified);
				}
			}
		);
	}
}


int Site::get_content_modified_time(const std::string& path) {
	// Check if already downloaded
	auto content = read_content(path);
	if(
		content &&
		content->IsObject() &&
		(*content)["modified"].IsInt()
	) {
		return (*content)["modified"].GetInt();
	} else {
		return 0;
	}
}


void Site::update_content(const std::string& path, int time_modified) {
	// clang-tidy doesn't work well with assert
	assert( // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
		(path.size() >= 13 && path.substr(path.size() - 13) == "/content.json") ||
		path == "content.json"
	);

	if(time_modified > 0 && time_modified <= get_content_modified_time(path)) {
		return;
	}

	bad_files[path]++;

	if(bad_files[path] >= 15) {
		return;
	}

	auto this_ptr = std::weak_ptr(shared_from_this());
	size_t limit = 1024 * 1024;

	with_worker(
		100 - static_cast<int>(std::min(size_t{100}, path.size())),
		path,
		&Peer::read_file, this_ptr, path, 0, limit, std::nullopt,
		[limit, path, time_modified, this_ptr](std::optional<Blob> data) {
			if(this_ptr.expired()) {
				return;
			}
			auto ptr = this_ptr.lock();

			if(!data) {
				ptr->update_content(path, time_modified);
				return;
			}

			if(data->size() == limit) {
				std::cerr << "Too big content.json" << std::endl;
				ptr->update_content(path, time_modified);
				return;
			}

			// Get rules for content.json
			auto rules = ptr->get_rules(path);

			// Verify content.json
			std::string content_char(data->size(), '\0');
			std::copy(data->begin(), data->end(), content_char.begin());
			if(!verify_content(content_char, rules.allowed_signers, rules.signers_required)) {
				ptr->update_content(path, time_modified);
				return;
			}

			// Check if newer than ours
			rapidjson::Document doc;
			doc.Parse(content_char.c_str());
			if(!doc.HasMember("modified") || !doc["modified"].IsInt() || doc["modified"].GetInt() <= ptr->get_content_modified_time(path)) {
				ptr->update_content(path, time_modified);
				return;
			}

			auto old_content_opt = ptr->read_content(path);

			std::filesystem::create_directories(ptr->get_path(path).remove_filename());
			std::ofstream stream(ptr->get_path(path));
			// FIXME: UBSan often fails on the next line
			if(!stream.good()) {
				return;
			}
			stream << content_char;
			if(stream.good()) {
				ptr->emitter.publish(SiteFileDoneEvent{path});

				if(doc["modified"].GetInt() >= time_modified) {
					std::cerr << "Updated " << path << " to the latest version!" << std::endl;
					ptr->bad_files[path] = 0;
				} else {
					std::cerr << "Updated " << path << " to an old version!" << std::endl;
				}

				// Check if some files should be updated
				std::map<std::string, decltype(doc[""])&> old_files;
				if(old_content_opt && old_content_opt->IsObject() && old_content_opt->HasMember("files")) {
					auto old_files_json = std::move((*old_content_opt)["files"]);
					for(auto it = old_files_json.MemberBegin(); it != old_files_json.MemberEnd(); ++it) {
						if(
							it->value.IsObject() &&
							it->value.HasMember("sha512") &&
							it->value["sha512"].IsString() &&
							it->value.HasMember("size") &&
							it->value["size"].IsInt()
						) {
							old_files.insert({it->name.GetString(), it->value});
						}
					}
				}

				if(doc.HasMember("files") && doc["files"].IsObject()) {
					for(auto it = doc["files"].MemberBegin(); it != doc["files"].MemberEnd(); ++it) {
						if(
							it->value.IsObject() &&
							it->value.HasMember("sha512") &&
							it->value["sha512"].IsString() &&
							it->value.HasMember("size") &&
							it->value["size"].IsInt()
						) {
							auto inner_path = it->name.GetString();
							std::string old_sha512;
							if(old_files.count(inner_path) != 0) {
								old_sha512 = old_files.at(inner_path)["sha512"].GetString();
							}
							auto new_sha512 = it->value["sha512"].GetString();
							if(new_sha512 != old_sha512) {
								auto file_path = (
									path == "content.json"
										? inner_path
										: path.substr(0, path.rfind('/')) + inner_path
								);
								std::cerr << "Downloading " << file_path << " is required" << std::endl;
								ptr->update_file(file_path, new_sha512, it->value["size"].GetInt());
							}
						}
					}
				}
			}
		}
	);
}


void Site::update_file(const std::string& path, const std::string& sha512, size_t size) {
	bad_files[path]++;

	if(bad_files[path] >= 15) {
		return;
	}

	auto this_ptr = std::weak_ptr(shared_from_this());
	auto tmp_file = std::make_shared<TempFile>();
	auto stream = std::make_shared<std::ofstream>(tmp_file->get_path(), std::ofstream::binary);

	with_worker(
		80 - static_cast<int>(std::min(size_t{80}, path.size())),
		path,
		&Peer::stream_file, this_ptr, path, stream, size, 0,
		[path, sha512, size, tmp_file, this_ptr](bool success) {
			if(this_ptr.expired()) {
				return;
			}
			auto ptr = this_ptr.lock();

			if(!success) {
				ptr->update_file(path, sha512, size);
				return;
			}

			if(std::filesystem::file_size(tmp_file->get_path()) != size) {
				ptr->update_file(path, sha512, size);
				return;
			}

			// Compare checksum
			SHA512_CTX ctx;
			if(SHA512_Init(&ctx) == 0) {
				ptr->update_file(path, sha512, size);
				return;
			}
			std::ifstream stream(tmp_file->get_path(), std::ifstream::binary);
			std::array<char, 4096> buffer{};
			while(true) {
				stream.read(buffer.data(), buffer.size());
				if(stream.gcount() == 0) {
					break;
				}
				// reinterpret_cast is forced by OpenSSL conventions
				// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
				if(SHA512_Update(&ctx, reinterpret_cast<unsigned char*>(buffer.data()), stream.gcount()) == 0) {
					ptr->update_file(path, sha512, size);
					return;
				}
			}
			if(!stream.good() && !stream.eof()) {
				ptr->update_file(path, sha512, size);
				return;
			}
			std::array<unsigned char, 64> result{};
			if(SHA512_Final(result.data(), &ctx) == 0) {
				ptr->update_file(path, sha512, size);
				return;
			}

			std::string sha512_hex;
			auto alphabet = "0123456789abcdef";
			for(int i = 0; i < 32; i++) {
				// It's obvious no array overrun is possible. Signed bitwise operation is a false positive
				// NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index, cppcoreguidelines-pro-bounds-pointer-arithmetic, hicpp-signed-bitwise)
				sha512_hex.push_back(alphabet[(result[i] >> 4U) & 15U]);
				// NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index, cppcoreguidelines-pro-bounds-pointer-arithmetic)
				sha512_hex.push_back(alphabet[result[i] & 15U]);
			}
			if(sha512_hex != sha512) {
				ptr->update_file(path, sha512, size);
				return;
			}

			std::filesystem::create_directories(ptr->get_path(path).remove_filename());
			tmp_file->move(ptr->get_path(path));
			ptr->emitter.publish(SiteFileDoneEvent{path});
		}
	);
}


std::optional<rapidjson::Document> Site::read_content(const std::string& path) {
	std::ifstream stream(get_path(path));
	if(!stream.good()) {
		return std::nullopt;
	}
	rapidjson::Document doc;
	rapidjson::IStreamWrapper wrapper(stream);
	doc.ParseStream(wrapper);
	if(doc.HasParseError()) {
		return std::nullopt;
	}
	return doc;
}


std::string Site::get_content_of(std::string path) {
	while(!path.empty()) {
		std::string filename;
		auto pos = path.rfind('/');
		if(pos == std::string::npos) {
			filename = path;
			path = "";
		} else {
			filename = path.substr(pos + 1);
			path = path.substr(0, pos);
		}

		auto content_path = path + (path.empty() ? "" : "/") + "content.json";
		if(read_content(content_path)) {
			return content_path;
		}
	}

	// Site was not downloaded yet
	return "content.json";
}


FileRules Site::get_rules(std::string path) {
	if(path == "content.json") {
		return {
			{get_case_sensitive_address()},
			1
		};
	}

	auto orig_path = path;

	while(!path.empty()) {
		std::string filename;
		auto pos = path.rfind('/');
		if(pos == std::string::npos) {
			filename = path;
			path = "";
		} else {
			filename = path.substr(pos + 1);
			path = path.substr(0, pos);
		}
		if(filename == "content.json") {
			continue;
		}
		auto content_path = path + (path.empty() ? "" : "/") + "content.json";

		auto content = read_content(content_path);
		if(!content) {
			continue;
		}

		auto prefix = path + (path.empty() ? "" : "/");
		// clang-tidy doesn't work well with assert
		assert(orig_path.substr(0, prefix.size()) == prefix); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
		auto relative_path = orig_path.substr(prefix.size());

		if(content->IsObject() && content->HasMember("includes")) {
			auto& includes = (*content)["includes"];
			if(includes.IsObject() && includes.HasMember(relative_path.c_str())) {
				auto& rules = includes[relative_path.c_str()];
				if(rules.IsObject()) {
					std::vector<std::string> signers;
					if(rules.HasMember("signers") && rules["signers"].IsArray()) {
						for(size_t i = 0; i < rules["signers"].Size(); i++) {
							auto& address = rules["signers"][i];
							if(address.IsString()) {
								signers.emplace_back(address.GetString());
							}
						}
					}
					signers.push_back(get_case_sensitive_address());

					int signers_required = (
						rules.HasMember("signers_required") && rules["signers_required"].IsInt()
							? rules["signers_required"].GetInt()
							: 1
					);

					return {signers, signers_required};
				}
			}
		}
	}

	// No content.json yet
	return {{}, 1};
}


std::filesystem::path Site::get_path(const std::string& inner_path) {
	return std::filesystem::path("data") / address / inner_path;
}


void Site::release_worker(const PeerAddress& addr) {
	if(worker_callback_queue.empty()) {
		free_peers.insert(addr);
	} else {
		if(peers.count(addr) == 0) {
			return;
		}
		auto callback = worker_callback_queue.top();
		worker_callback_queue.pop();
		callback.f(peers.at(addr));
	}
}


void Site::release_worker_soon(const PeerAddress& addr) {
	auto this_ptr = std::weak_ptr(shared_from_this());

	// Delay worker release
	auto loop = uvw::Loop::getDefault();
	auto timer = loop->resource<uvw::TimerHandle>();
	timer->once<uvw::TimerEvent>([this_ptr, addr](const uvw::TimerEvent&, uvw::TimerHandle& timer) {
		timer.clear();
		timer.close();
		if(!this_ptr.expired()) {
			this_ptr.lock()->release_worker(addr);
		}
	});
	timer->start(std::chrono::milliseconds{1}, std::chrono::milliseconds{0});
}


template<typename... ArgsAll, typename Function, size_t... I>
void Site::_with_worker_impl(int priority, std::shared_ptr<TaskHandle> handle, std::tuple_element_t<sizeof...(ArgsAll) - 1, std::tuple<ArgsAll...>> callback, Function func, std::index_sequence<I...> index, ArgsAll... args_all) {
	auto this_ptr = std::weak_ptr(shared_from_this());

	auto new_callback = [this_ptr, priority, handle = std::move(handle), callback = std::move(callback), func, index, args_all...](const std::shared_ptr<Peer>& peer) {
		if(this_ptr.expired()) {
			return;
		}
		auto ptr = this_ptr.lock();

		if(handle->is_completed()) {
			ptr->release_worker_soon(peer->addr);
			return;
		}

		peer->add_task(handle);

		auto loop = uvw::Loop::getDefault();
		auto timer = loop->resource<uvw::TimerHandle>();
		timer->once<uvw::CloseEvent>([](const uvw::CloseEvent&, uvw::TimerHandle& timer) {
			timer.clear();
		});
		timer->once<uvw::TimerEvent>([handle, this_ptr, priority, callback, func, index, args_all...](const uvw::TimerEvent&, uvw::TimerHandle& timer) mutable {
			if(!this_ptr.expired() && !handle->is_completed()) {
				// Spawn yet another peer
				this_ptr.lock()->_with_worker_impl(priority - 5, handle, callback, func, index, std::forward<ArgsAll>(args_all)...);
			}
			timer.close();
		});
		timer->start(std::chrono::milliseconds{5000}, std::chrono::milliseconds{0});

		(peer.get()->*func)(std::get<I>(std::tuple<ArgsAll...>(args_all...))..., [callback, handle, timer](auto... args) {
			handle->mark_completed();
			callback(args...);
			timer->close();
		});
	};

	if(free_peers.empty()) {
		// Try announcing to get more peers
		check_peers();
		worker_callback_queue.emplace(priority, new_callback);
	} else {
		auto addr = free_peers.get_random_key();
		assert(peers.at(addr)->current_task.expired()); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
		free_peers.erase(addr);
		new_callback(peers.at(addr));
	}
}


template<typename... Args, typename Function>
void Site::with_worker(int priority, const std::string& description, Function func, Args... args) {
	using Callback = std::tuple_element_t<sizeof...(Args) - 1, std::tuple<Args...>>;

	auto this_ptr = std::weak_ptr(shared_from_this());

	auto deleter = [this_ptr](const std::shared_ptr<Peer>& peer) {
		if(!this_ptr.expired()) {
			this_ptr.lock()->release_worker_soon(peer->addr);
		}
	};

	auto handle = std::make_shared<TaskHandle>(address + ": " + description, deleter);

	_with_worker_impl(
		priority, handle,
		std::forward<Callback>(std::get<sizeof...(Args) - 1>(std::tuple<Args...>(args...))),
		func, std::make_index_sequence<sizeof...(Args) - 1>{}, args...
	);
}


WorkerCallback::WorkerCallback(int priority, std::function<void(std::shared_ptr<Peer>)> f): priority(priority), f(std::move(f)) {
}


bool WorkerCallback::operator<(const WorkerCallback& other) const {
	return priority < other.priority;
}