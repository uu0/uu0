#ifndef FRONTEND_WEBSOCKETHANDLER_HPP
#define FRONTEND_WEBSOCKETHANDLER_HPP


#include "site/site.hpp"
#include "util/json.hpp"
#include "websocketrequest.hpp"
#include <map>
#include <optional>
#include <rapidjson/document.h>
#include <stdexcept>
#include <string>
#include <vector>


class WebSocketHandler {
	WebSocketRequest& request;
	std::optional<SiteEmitter::Connection<SiteFileDoneEvent>> site_file_done_event;

	static std::map<std::string, std::function<void(WebSocketHandler*, const rapidjson::Value&)>> action_mapping;

	[[nodiscard]] JSONObject get_site_info() const;

	void send_error(const std::string& text);
	void send_cmd(const std::string& cmd, JSONObject&& params);


	//
	// Argument parser
	// Usage: auto [value1, value2, value3] = get_params<T1, T2, T3>(params, name1, Default{name2, default2}, Default{name3, default3});
	//
	template<typename T> struct Default {
		const char* name;
		T def;
		Default(const char* name, T&& def): name(name), def(std::forward<T>(def)) {
		}
	};

	// _parse_rapidjson_value is a specialized private function with no default behavior
	// NOLINTNEXTLINE(modernize-use-equals-delete)
	template<typename T> T _parse_rapidjson_value(const rapidjson::Value& value) = delete;

	template<> int _parse_rapidjson_value<int>(const rapidjson::Value& value) {
		if(value.IsInt()) {
			return value.GetInt();
		} else {
			throw std::invalid_argument("Value is not an integer");
		}
	}

	template<> double _parse_rapidjson_value<double>(const rapidjson::Value& value) {
		if(value.IsDouble()) {
			return value.GetDouble();
		} else {
			throw std::invalid_argument("Value is not a double");
		}
	}

	template<> std::string _parse_rapidjson_value<std::string>(const rapidjson::Value& value) {
		if(value.IsString()) {
			return value.GetString();
		} else {
			throw std::invalid_argument("Value is not a string");
		}
	}

	template<> bool _parse_rapidjson_value<bool>(const rapidjson::Value& value) {
		if(value.IsBool()) {
			return value.GetBool();
		} else {
			throw std::invalid_argument("Value is not a bool");
		}
	}


	template<typename T> T _get_rapidjson_value_array(const rapidjson::Value& params, size_t index, const char* name) {
		if(index < params.Size()) {
			return _parse_rapidjson_value<T>(params[index]);
		} else {
			throw std::invalid_argument(std::string("'") + name + "' argument is missing");
		}
	}

	template<typename T> T _get_rapidjson_value_array(const rapidjson::Value& params, size_t index, const Default<T>& name) {
		if(index < params.Size()) {
			return _parse_rapidjson_value<T>(params[index]);
		} else {
			return name.def;
		}
	}


	template<typename T> T _get_rapidjson_value_object(const rapidjson::Value& params, const char* name) {
		if(params.HasMember(name)) {
			return _parse_rapidjson_value<T>(params[name]);
		} else {
			throw std::invalid_argument(std::string("'") + name + "' argument is missing");
		}
	}

	template<typename T> T _get_rapidjson_value_object(const rapidjson::Value& params, const Default<T>& name) {
		if(params.HasMember(name.name)) {
			return _parse_rapidjson_value<T>(params[name.name]);
		} else {
			return name.def;
		}
	}


	template<typename... Types, typename... Names, size_t... I>
	std::tuple<Types...> _get_params_impl(const rapidjson::Value& params, std::index_sequence<I...>, Names... names) {
		static_assert(sizeof...(names) == sizeof...(Types));
		std::tuple<Names...> names_tuple{names...};
		if(params.IsArray()) {
			return std::tuple<Types...>(_get_rapidjson_value_array<std::tuple_element_t<I, std::tuple<Types...>>>(params, I, std::get<I>(names_tuple))...);
		} else if(params.IsObject()) {
			return std::tuple<Types...>(_get_rapidjson_value_object<std::tuple_element_t<I, std::tuple<Types...>>>(params, std::get<I>(names_tuple))...);
		} else {
			throw std::logic_error("Invalid parameter type");
		}
	}

	template<typename... Types, typename... Names>
	std::tuple<Types...> get_params(const rapidjson::Value& params, Names... names) {
		using I = std::make_index_sequence<sizeof...(Types)>;
		return _get_params_impl<Types...>(params, I{}, names...);
	}
	// End of argument parser


	void action_file_need(const rapidjson::Value& params);

public:
	std::string site_address;
	std::weak_ptr<Site> site;

	explicit WebSocketHandler(WebSocketRequest& request);
	WebSocketHandler(const WebSocketHandler&) = delete;
	// Move construction is dangerous because lambdas capture this
	WebSocketHandler(WebSocketHandler&&) = delete;
	~WebSocketHandler();
	WebSocketHandler& operator=(const WebSocketHandler&) = delete;
	WebSocketHandler& operator=(WebSocketHandler&&) = delete;
	void handle(const std::string& message);
};


#endif