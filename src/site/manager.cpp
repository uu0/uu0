#include "manager.hpp"
#include "site.hpp"


std::weak_ptr<Site> SiteManager::get(const std::string& address) {
	if(sites.count(address) == 0) {
		return {};
	} else {
		return std::weak_ptr(sites[address]);
	}
}


std::weak_ptr<Site> SiteManager::add(const std::string& address) {
	if(sites.count(address) == 0) {
		auto site = std::make_shared<Site>(address);
		sites.emplace(address, site);
		site->update();
	}
	return std::weak_ptr(sites.at(address));
}


std::vector<std::string> SiteManager::get_site_list() const {
	std::vector<std::string> list;
	for(auto& [address, _]: sites) {
		list.push_back(address);
	}
	return list;
}


SiteManager site_manager;