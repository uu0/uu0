#ifndef UTIL_BIGNUMBER_HPP
#define UTIL_BIGNUMBER_HPP


#include "blob.hpp"
#include <openssl/bn.h> // IWYU pragma: export
#include <openssl/ossl_typ.h>


class BigNumberContext {
	BN_CTX* bn_ctx = nullptr;

public:
	thread_local static BigNumberContext ctx;

	BigNumberContext();
	BigNumberContext(const BigNumberContext&) = delete;
	BigNumberContext(BigNumberContext&&) = delete;
	~BigNumberContext();
	BigNumberContext& operator=(const BigNumberContext&) = delete;
	BigNumberContext& operator=(BigNumberContext&&) = delete;
	static BN_CTX* get();
};


class BigNumber {
public:
	BIGNUM* bn;
	BigNumber();
	explicit BigNumber(BIGNUM* bn);
	explicit BigNumber(const Blob& blob);
	explicit BigNumber(BN_ULONG value);
	BigNumber(const BigNumber& other);
	BigNumber(BigNumber&& other) noexcept;
	~BigNumber();
	BigNumber& operator=(const BigNumber& other);
	BigNumber& operator=(BigNumber&& other) noexcept;
	[[nodiscard]] bool operator==(const BigNumber& other) const;
	[[nodiscard]] bool operator!=(const BigNumber& other) const;
	[[nodiscard]] bool operator<(const BigNumber& other) const;
	[[nodiscard]] bool operator>(const BigNumber& other) const;
	[[nodiscard]] bool operator<=(const BigNumber& other) const;
	[[nodiscard]] bool operator>=(const BigNumber& other) const;
	[[nodiscard]] BigNumber operator+(const BigNumber& other) const;
	[[nodiscard]] BigNumber operator-(const BigNumber& other) const;
	[[nodiscard]] BigNumber operator*(const BigNumber& other) const;
	[[nodiscard]] BigNumber operator/(const BigNumber& other) const;
	[[nodiscard]] BigNumber operator%(const BigNumber& other) const;
	[[nodiscard]] BigNumber inverse(const BigNumber& modulo) const;
	[[nodiscard]] BigNumber operator-() const;
	[[nodiscard]] explicit operator BN_ULONG() const;
	[[nodiscard]] Blob bytes(size_t sz) const;
	[[nodiscard]] size_t size() const;
	[[nodiscard]] std::string to_string() const;
};


BigNumber operator"" _bn(const char* c_str);


#endif