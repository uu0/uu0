#ifndef PEER_PEER_HPP
#define PEER_PEER_HPP


#include "address.hpp"
#include "util/blob.hpp"
#include <fstream>
#include <map>
#include <ofats/invocable.h>


class Peer;
class Site;


struct PeerConnectingEvent {
};

struct PeerConnectedEvent {
};

struct PeerDisconnectedEvent {
};


class PeerEmitter: public uvw::Emitter<PeerEmitter> {
	friend class Peer;
};


class TaskHandle {
	std::vector<std::weak_ptr<Peer>> peers;
	std::function<void(const std::shared_ptr<Peer>&)> deleter;
	bool completed = false;

public:
	std::string subject;

	TaskHandle(std::string subject, const std::function<void(const std::shared_ptr<Peer>&)>& deleter);
	TaskHandle(const TaskHandle&) = delete;
	TaskHandle(TaskHandle&&) = delete;
	TaskHandle& operator=(const TaskHandle&) = delete;
	TaskHandle& operator=(TaskHandle&&) = delete;

	void add_peer(std::weak_ptr<Peer> peer);
	void mark_completed();
	bool is_completed() const;
};


class Peer: public std::enable_shared_from_this<Peer> {
public:
	PeerAddress addr;
	PeerEmitter emitter;
	std::string origin;
	std::weak_ptr<TaskHandle> current_task;

	Peer(const Peer&) = delete;
	Peer(Peer&&) = delete;
	Peer(PeerAddress addr, std::string origin);
	Peer& operator=(const Peer&) = delete;
	Peer& operator=(Peer&&) = delete;
	~Peer() = default;

	void connect();
	bool is_connectable() const;
	bool is_connecting() const;
	bool is_connected() const;

	void add_task(std::shared_ptr<TaskHandle> handle);

	void pex(const std::weak_ptr<Site>& site, ofats::any_invocable<void(std::vector<PeerAddress>)>&& callback);
	void list_modified(const std::weak_ptr<Site>& site, int since, ofats::any_invocable<void(std::map<std::string, int>)>&& callback);
	void read_file(const std::weak_ptr<Site>& site, const std::string& path, size_t location, size_t limit, std::optional<size_t> size, ofats::any_invocable<void(std::optional<Blob>)>&& callback);
	void stream_file(const std::weak_ptr<Site>& site, const std::string& path, const std::shared_ptr<std::ofstream>& stream, std::optional<size_t> size, size_t location, ofats::any_invocable<void(bool)>&& callback);
};


#endif