#include "frontend/httprequest.hpp"
#include "router.hpp"
#include "site/manager.hpp"
#include "site/site.hpp"
#include <filesystem>
#include <iostream>


FrontendRouter::FrontendRouter(HTTPRequest& http): http(http) {
}


void FrontendRouter::route() {
	if(http.url.size() >= 7 && http.url.substr(0, 7) == "http://") {
		// Parse request URL
		auto path = http.url.substr(7);
		auto idx = path.find('/');
		if(idx == std::string::npos) {
			http.send_head("400 Bad Request", {
				{"Content-Type", "text/html"},
				{"Transfer-Encoding", "chunked"}
			});
			http.send_chunk("<h1>400 Bad Request</h1><p>Missing trailing slash</p>");
			http.end();
			return;
		}
		auto host = path.substr(0, idx);
		path = path.substr(idx + 1);
		if(host.size() < 8 || host.substr(host.size() - 8) != ".zeronet") {
			http.send_head("400 Bad Request", {
				{"Content-Type", "text/html"},
				{"Transfer-Encoding", "chunked"}
			});
			http.send_chunk("<h1>400 Bad Request</h1><p>Accessing non-ZeroNet sites via ZeroNet proxy</p>");
			http.end();
			return;
		}
		auto site_address = host.substr(0, host.size() - 8);

		if(site_address == "home") {
			// Redirect to home site (ZeroHello for now, should be changeable)
			http.send_head("307 Temporary Redirect", {
				{"Content-Type", "text/html"},
				{"Transfer-Encoding", "chunked"},
				{"Location", "http://0hello4uzjaletfx6nh3pmwfp3qbrbtf3dlqf5lug.zeronet/"}
			});
			http.send_chunk("You are being redirected to home site...");
			http.end();
			return;
		}

		auto site = site_manager.get(site_address);
		if(site.expired()) {
			// Downloading the site is required
			http.output_file("assets/frontend/downloading.html");
			return;
		}

		std::cout << site_address << " " << path << std::endl;
	} else if(http.url == "/") {
		http.output_file("assets/frontend/home.html");
	} else if(http.url.size() >= 25 && http.url.substr(0, 25) == "/ZeroNet-Internal/assets/") {
		std::filesystem::path assets("assets");
		std::filesystem::path path;
		try {
			path = canonical(assets / http.url.substr(25));
			auto relative = path.lexically_relative(canonical(assets));
			if(relative.has_root_path() || relative.empty() || *relative.begin() == "..") {
				http.send_head("403 Forbidden", {
					{"Content-Type", "text/html"},
					{"Transfer-Encoding", "chunked"}
				});
				http.send_chunk("<h1>403 Forbidden</h1><p>Accessing file outside assets/</p>");
				http.end();
				return;
			}
		} catch(std::filesystem::filesystem_error& e) {
			http.send_head("500 Internal Server Error", {
				{"Content-Type", "text/html"},
				{"Transfer-Encoding", "chunked"}
			});
			http.send_chunk("<h1>500 Internal Server Error</h1><p>");
			http.send_chunk(e.what());
			http.send_chunk("</p>");
			http.end();
			return;
		}

		http.output_file(path);
	} else if(http.url == "/ZeroNet-Internal/WebSocket") {
		http.send_head("400 Bad Request", {
			{"Content-Type", "text/html"},
			{"Transfer-Encoding", "chunked"}
		});
		http.send_chunk("<h1>400 Bad Request</h1><p>/ZeroNet-Internal/WebSocket can only be accessed via WebSocket protocol</p>");
		http.end();
	} else if(http.url == "/ZeroNet-Internal/Stats") {
		http.send_head("200 OK", {
			{"Content-Type", "text/html"},
			{"Transfer-Encoding", "chunked"}
		});

		for(const auto& site_address: site_manager.get_site_list()) {
			http.send_chunk("<h2>" + site_address + "</h2>");
			auto site = site_manager.get(site_address).lock();

			http.send_chunk("<h3>Connected</h3>");
			http.send_chunk("<pre>");
			for(auto& [peer_address, peer]: site->peers) {
				if(peer->is_connected()) {
					if(peer->current_task.expired()) {
						http.send_chunk(peer_address.to_string() + " [idle]\n");
					} else {
						http.send_chunk(peer_address.to_string() + " [working] " + peer->current_task.lock()->subject + "\n");
					}
				}
			}
			http.send_chunk("</pre>");

			http.send_chunk("<h3>Disconnected</h3>");
			http.send_chunk("<pre>");
			for(auto& [peer_address, peer]: site->peers) {
				if(!peer->is_connected()) {
					if(peer->is_connectable()) {
						http.send_chunk(peer_address.to_string() + " [connectable]\n");
					} else {
						http.send_chunk(peer_address.to_string() + " [disconnected]\n");
					}
				}
			}
			http.send_chunk("</pre>");
		}

		http.end();
	} else {
		auto address = http.url.substr(1);
		std::string path;

		auto pos = address.find('/', 1);
		if(pos != std::string::npos) {
			path = address.substr(pos + 1);
			address = address.substr(0, pos);
		}

		if(Site::is_valid_case_sensitive_address(address)) {
			address = Site::case_sensitive_address_to_address(address);
		}

		if(Site::is_valid_address(address)) {
			// Redirect
			http.send_head("301 Moved Permanently", {
				{"Content-Type", "text/html"},
				{"Transfer-Encoding", "chunked"},
				{"Location", "http://" + address + ".zeronet/" + path}
			});
			http.send_chunk("You are being redirected to the site...");
			http.end();
			return;
		}

		http.send_head("404 File Not Found", {
			{"Content-Type", "text/html"},
			{"Transfer-Encoding", "chunked"}
		});
		http.send_chunk("<h1>404 File Not Found</h1>");
		http.end();
	}
}