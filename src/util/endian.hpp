#ifndef UTIL_ENDIAN_HPP
#define UTIL_ENDIAN_HPP


uint64_t ntohq(uint64_t num);
uint64_t htonq(uint64_t num);


#endif