#include "httprequest.hpp"
#include <fstream> // IWYU pragma: keep


HTTPRequest::HTTPRequest(std::shared_ptr<uvw::TCPHandle> tcp): tcp(std::move(tcp)) {
}


void HTTPRequest::send(const Blob& data) {
	// char[] is forced by uvw, wrapping it in unique_ptr is the best we can achieve
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto ptr = std::make_unique<char[]>(data.size());
	std::copy(data.begin(), data.end(), ptr.get());
	tcp->write(std::move(ptr), data.size());
}


void HTTPRequest::send(Blob&& data) {
	auto blob_ptr = std::make_unique<Blob>(std::move(data));
	auto raw_data_ptr = reinterpret_cast<char*>(blob_ptr->data());
	auto size = blob_ptr->size();
	auto deleter = [blob_ptr = std::move(blob_ptr)](char*) {
	};
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto data_ptr = std::unique_ptr<char[], decltype(deleter)>(raw_data_ptr, std::move(deleter));
	tcp->write(std::move(data_ptr), size);
}


void HTTPRequest::send(const std::string& data) {
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto ptr = std::make_unique<char[]>(data.size());
	std::copy(data.begin(), data.end(), ptr.get());
	tcp->write(std::move(ptr), data.size());
}


void HTTPRequest::send(std::string&& data) {
	auto str_ptr = std::make_unique<std::string>(std::move(data));
	auto raw_data_ptr = str_ptr->data();
	auto size = str_ptr->size();
	auto deleter = [str_ptr = std::move(str_ptr)](char*) {
	};
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto data_ptr = std::unique_ptr<char[], decltype(deleter)>(raw_data_ptr, std::move(deleter));
	tcp->write(std::move(data_ptr), size);
}


void HTTPRequest::send_chunk(const Blob& data) {
	if(data.empty()) {
		return;
	}

	// Prefix: count in hex + "\r\n"
	std::stringstream ss;
	ss << std::hex << std::uppercase << data.size();
	auto prefix = ss.str() + "\r\n";
	// char[] is forced by uvw, wrapping it in unique_ptr is the best we can achieve
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto prefix_ptr = std::make_unique<char[]>(prefix.size());
	std::copy(prefix.begin(), prefix.end(), prefix_ptr.get());
	send(std::move(prefix_ptr), prefix.size());

	// Real data
	send(data);

	// Suffix: "\r\n"
	send("\r\n"_blob);
}


void HTTPRequest::send_chunk(Blob&& data) {
	if(data.empty()) {
		return;
	}

	// Prefix: count in hex + "\r\n"
	std::stringstream ss;
	ss << std::hex << std::uppercase << data.size();
	auto prefix = ss.str() + "\r\n";
	// char[] is forced by uvw, wrapping it in unique_ptr is the best we can achieve
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto prefix_ptr = std::make_unique<char[]>(prefix.size());
	std::copy(prefix.begin(), prefix.end(), prefix_ptr.get());
	send(std::move(prefix_ptr), prefix.size());

	// Real data
	send(std::move(data));

	// Suffix: "\r\n"
	send("\r\n"_blob);
}


void HTTPRequest::send_chunk(const std::string& data) {
	if(data.empty()) {
		return;
	}

	std::stringstream ss;
	ss << std::hex << std::uppercase << data.size();
	auto prefix = ss.str() + "\r\n";
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto prefix_ptr = std::make_unique<char[]>(prefix.size());
	std::copy(prefix.begin(), prefix.end(), prefix_ptr.get());
	send(std::move(prefix_ptr), prefix.size());
	send(data);
	send("\r\n"_blob);
}


void HTTPRequest::send_chunk(std::string&& data) {
	if(data.empty()) {
		return;
	}

	std::stringstream ss;
	ss << std::hex << std::uppercase << data.size();
	auto prefix = ss.str() + "\r\n";
	// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
	auto prefix_ptr = std::make_unique<char[]>(prefix.size());
	std::copy(prefix.begin(), prefix.end(), prefix_ptr.get());
	send(std::move(prefix_ptr), prefix.size());
	send(std::move(data));
	send("\r\n"_blob);
}


void HTTPRequest::send_head(const std::string& status, const std::map<std::string, std::string>& headers) {
	send("HTTP/1.1 "_blob);
	send(status);
	send("\r\n"_blob);
	for(auto [key, value]: headers) {
		send(key);
		send(": "_blob);
		send(value);
		send("\r\n"_blob);
	}
	send("\r\n"_blob);
}


void HTTPRequest::end() {
	send("0\r\n\r\n"_blob);
}


void HTTPRequest::close() {
	tcp->close();
}


void HTTPRequest::send_file(const std::filesystem::path& path) {
	std::ifstream in(path, std::ifstream::binary);

	while(in) {
		// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
		Blob buf(4096, 0);
		in.read(reinterpret_cast<char*>(buf.data()), buf.size());
		buf.resize(in.gcount());
		send_chunk(std::move(buf));
	}
}


void HTTPRequest::output_file(const std::filesystem::path& path) {
	send_head("200 OK", {
		{"Content-Type", extension_to_mime(path.extension())},
		{"Transfer-Encoding", "chunked"}
	});
	send_file(path);
	end();
}


std::string HTTPRequest::extension_to_mime(std::string ext) {
	static std::map<std::string, std::string> mapping{
		{".7z", "application/x-7z-compressed"},
		{".sfw", "application/x-shockwave-flash"},
		{".pdf", "application/pdf"},
		{".aac", "audio/x-aac"},
		{".dmg", "application/x-apple-diskimage"},
		{".s", "text/x-asm"},
		{".aif", "audio/x-aiff"},
		{".avi", "video/x-msvideo"},
		{".bmp", "image/bmp"},
		{".torrent", "application/x-bittorrent"},
		{".sh", "application/x-sh"},
		{".bz", "application/x-bzip"},
		{".bz2", "application/x-bzip2"},
		{".csh", "application/x-csh"},
		{".c", "text/x-c"},
		{".css", "text/css"},
		{".csv", "text/csv"},
		{".deb", "application/x-debian-package"},
		{".dvi", "application/x-dvi"},
		{".djvu", "image/vnd.djvu"},
		{".epub", "application/epub+zip"},
		{".f", "text/x-fortran"},
		{".h261", "video/h261"},
		{".h263", "video/h263"},
		{".h264", "video/h264"},
		{".html", "text/html"},
		{".jar", "application/java-archive"},
		{".class", "application/java-vm"},
		{".java", "text/x-java-source,java"},
		{".js", "application/javascript"},
		{".json", "application/json"},
		{".jpeg", "image/jpeg"},
		{".jpg", "image/jpeg"},
		{".jpgv", "video/jpeg"},
		{".latex", "application/x-latex"},
		{".m3u", "audio/x-mpegurl"},
		{".m4v", "video/x-m4v"},
		{".mathml", "application/mathml+xml"},
		{".exe", "application/x-msdownload"},
		{".cab", "application/vnd.ms-cab-compressed"},
		{".xls", "application/vnd.ms-excel"},
		{".chm", "application/vnd.ms-htmlhelp"},
		{".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
		{".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
		{".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
		{".ppt", "application/vnd.ms-powerpoint"},
		{".wm", "video/x-ms-wm"},
		{".wma", "audio/x-ms-wma"},
		{".doc", "application/msword"},
		{".mpeg", "video/mpeg"},
		{".mp4a", "audio/mp4"},
		{".mp4", "video/mp4"},
		{".m3u8", "application/vnd.apple.mpegurl"},
		{".ogx", "application/ogg"},
		{".oga", "audio/ogg"},
		{".ogv", "video/ogg"},
		{".weba", "audio/webm"},
		{".webm", "video/webm"},
		{".odc", "application/vnd.oasis.opendocument.chart"},
		{".odb", "application/vnd.oasis.opendocument.database"},
		{".odf", "application/vnd.oasis.opendocument.formula"},
		{".odg", "application/vnd.oasis.opendocument.graphics"},
		{".odi", "application/vnd.oasis.opendocument.image"},
		{".odp", "application/vnd.oasis.opendocument.presentation"},
		{".ods", "application/vnd.oasis.opendocument.spreadsheet"},
		{".odt", "application/vnd.oasis.opendocument.text"},
		{".otf", "application/x-font-otf"},
		{".p", "text/x-pascal"},
		{".png", "image/png"},
		{".psd", "image/vnd.adobe.photoshop"},
		{".ai", "application/postscript"},
		{".qt", "video/quicktime"},
		{".rar", "application/x-rar-compressed"},
		{".rss", "application/rss+xml"},
		{".svg", "image/svg+xml"},
		{".tiff", "image/tiff"},
		{".tar", "application/x-tar"},
		{".tcl", "application/x-tcl"},
		{".tex", "application/x-tex"},
		{".txt", "text/plain"},
		{".t", "text/troff"},
		{".ttf", "application/x-font-ttf"},
		{".wav", "audio/x-wav"},
		{".woff", "application/x-font-woff"},
		{".webp", "image/webp"},
		{".xhtml", "application/xhtml+xml"},
		{".xml", "application/xml"},
		{".yaml", "text/yaml"},
		{".zip", "application/zip"}
	};

	std::transform(ext.begin(), ext.end(), ext.begin(), [](unsigned char c) {
		return std::tolower(c);
	});
	if(mapping.count(ext) == 0) {
		return "application/octet-stream";
	} else {
		return mapping[ext];
	}
}