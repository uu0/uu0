#include "address.hpp"
#include "announcer/announcer.hpp"
#include "connection.hpp"
#include "message.hpp"
#include "util/blobmsgpack.hpp"
#include "util/msgpack.hpp"
#include <cassert>
#include <cstring>
#include <iostream>


Connection::Connection(ConnectionManager& manager, PeerAddress addr): manager(manager), addr(std::move(addr)), established(false) {
}


void Connection::connect() {
	auto loop = uvw::Loop::getDefault();

	auto this_ptr = std::weak_ptr(shared_from_this());

	tcp = loop->resource<uvw::TCPHandle>();
	auto timer = loop->resource<uvw::TimerHandle>();

	tcp->once<uvw::CloseEvent>([timer](const uvw::CloseEvent&, uvw::TCPHandle& tcp) {
		tcp.clear();
		timer->close();
	});

	auto error_handler = tcp->on<uvw::ErrorEvent>([](const uvw::ErrorEvent&, uvw::TCPHandle& tcp) {
		// Error during connection
		tcp.close();
	});

	tcp->once<uvw::EndEvent>([this_ptr](const uvw::EndEvent&, uvw::TCPHandle& tcp) {
		tcp.close();
		if(!this_ptr.expired()) {
			auto ptr = this_ptr.lock();
			ptr->emitter.publish(ConnectionClosedEvent{});
			ptr->manager.remove(ptr);
		}
	});

	tcp->once<uvw::ConnectEvent>([this_ptr, timer, error_handler](const uvw::ConnectEvent&, uvw::TCPHandle& tcp) {
		tcp.erase(error_handler);
		timer->close();
		if(!this_ptr.expired()) {
			this_ptr.lock()->handle_connection();
		}
	});

	try {
		addr.visit([this](sockaddr* addr) {
			tcp->connect(*addr);
		});
	} catch(std::logic_error&) {
	}


	timer->once<uvw::CloseEvent>([](const uvw::CloseEvent&, uvw::TimerHandle& timer) {
		timer.clear();
	});

	timer->once<uvw::TimerEvent>([this_ptr](const uvw::TimerEvent&, uvw::TimerHandle& timer) {
		if(!this_ptr.expired()) {
			this_ptr.lock()->tcp->close();
		}
		timer.close();
	});

	timer->start(std::chrono::milliseconds{15000}, std::chrono::milliseconds{15000});
}


void Connection::close() {
	tcp->close();
}


int Connection::get_req_id() {
	return next_req_id++;
}


void Connection::handle_connection() {
	auto this_ptr = weak_from_this();

	tcp->on<uvw::DataEvent>([this_ptr](const uvw::DataEvent& data, uvw::TCPHandle& tcp) {
		if(this_ptr.expired()) {
			return;
		}
		auto ptr = this_ptr.lock();
		try {
			ptr->unpacker.reserve_buffer(data.length);
			memcpy(ptr->unpacker.buffer(), data.data.get(), data.length);
			ptr->unpacker.buffer_consumed(data.length);

			msgpack::object_handle handle;
			while(ptr->unpacker.next(handle)) {
				ptr->handle_message(handle.get());
			}
		} catch(msgpack::unpack_error&) {
			std::cout << "unpack error" << std::endl;
			tcp.close();
		}
	});
	tcp->on<uvw::EndEvent>([](const uvw::EndEvent&, uvw::TCPHandle&) {
		std::cout << "end event" << std::endl;
	});

	tcp->read();

	do_handshake();
}


void Connection::do_handshake() {
	send(ConnectionMessageHandshake{
		"handshake",
		0,
		ConnectionHandshake{
			"0.7.1",
			"v2",
			true,
			std::string(announcer.peer_id.begin(), announcer.peer_id.end()),
			1,
			false,
			addr.to_string(),
			6000,
			{},
			std::nullopt,
			time(nullptr),
			std::nullopt,
			std::nullopt
		}
	}, std::bind(&Connection::handle_handshake_response, this, std::placeholders::_1));
}


void Connection::handle_handshake_response(msgpack::object obj) {
	handshake = obj.as<ConnectionHandshake>();
	if(!established) {
		established = true;
		emitter.publish(ConnectionEstablishedEvent{});
	}
}


void Connection::handle_message(msgpack::object obj) {
	// Convert bin to str because that's what map adapater needs
	fix_msgpack_object(obj);

	try {
		auto abstract_message = obj.as<ConnectionAbstractMessage>();

		if(abstract_message.cmd == "response") {
			auto message = obj.as<ConnectionMessageResponse>();
			if(!message.to) {
				std::cout << "Invalid message" << std::endl;
				return;
			}
			int to = message.to.value();
			if(response_queue.count(to) == 0) {
				std::cout << "No one to reply" << std::endl;
				return;
			}
			auto callback = std::move(response_queue.at(to));
			response_queue.erase(to);
			callback(obj);
		}
	} catch(msgpack::v1::type_error& e) {
		std::cout << "Bad message " << e.what() << std::endl;
	}
}


std::shared_ptr<Connection> ConnectionManager::add(const PeerAddress& addr) {
	if(connections.count(addr) == 0) {
		auto conn = std::make_shared<Connection>(*this, addr);
		connections.insert({addr, conn});
		conn->connect();
	}
	return connections[addr];
}


std::weak_ptr<Connection> ConnectionManager::get(const PeerAddress& addr) {
	if(connections.count(addr) == 0) {
		return std::weak_ptr<Connection>();
	}
	return std::weak_ptr(connections[addr]);
}


void ConnectionManager::remove(const std::shared_ptr<Connection>& connection) {
	auto addr = connection->addr;
	if(connections.count(addr) != 0) {
		// clang-tidy doesn't work well with assert
		assert(connections[addr] == connection); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
		connections.erase(addr);
	}
}