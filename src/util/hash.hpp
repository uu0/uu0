#ifndef UTIL_HASH_HPP
#define UTIL_HASH_HPP


#include <openssl/ripemd.h>
#include <openssl/sha.h>


inline std::array<unsigned char, 20> sha1(const unsigned char* data, size_t size) {
	std::array<unsigned char, 20> out{};
	SHA1(data, size, out.data());
	return out;
}


inline std::array<unsigned char, 32> sha256(const unsigned char* data, size_t size) {
	std::array<unsigned char, 32> out{};
	SHA256(data, size, out.data());
	return out;
}


inline std::array<unsigned char, 20> ripemd160(const unsigned char* data, size_t size) {
	std::array<unsigned char, 20> out{};
	RIPEMD160(data, size, out.data());
	return out;
}


// We encapsulate reinterpret_cast in this header file because it's forced by OpenSSL


inline std::array<unsigned char, 20> sha1(const char* data, size_t size) {
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
	return sha1(reinterpret_cast<const unsigned char*>(data), size);
}


inline std::array<unsigned char, 32> sha256(const char* data, size_t size) {
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
	return sha256(reinterpret_cast<const unsigned char*>(data), size);
}


inline std::array<unsigned char, 20> ripemd160(const char* data, size_t size) {
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
	return ripemd160(reinterpret_cast<const unsigned char*>(data), size);
}


#endif