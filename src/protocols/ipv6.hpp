#ifndef PROTOCOLS_IPV6_HPP
#define PROTOCOLS_IPV6_HPP


#include "peer/address.hpp"
#include "util/blob.hpp"
#include <byteswap.h>
#include <cstring>


class PeerAddressIPv6: public PeerAddressBase {
protected:
	[[nodiscard]] bool is_less(const PeerAddressBase& other) const override {
		auto other_cast = dynamic_cast<const PeerAddressIPv6&>(other);
		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
		int cmp = std::memcmp(&ip.s6_addr, &other_cast.ip.s6_addr, sizeof(ip.s6_addr));
		if(cmp != 0) {
			return cmp < 0;
		}
		return ntohs(port) < ntohs(other_cast.port);
	}


	[[nodiscard]] bool is_equal(const PeerAddressBase& other) const override {
		auto other_cast = dynamic_cast<const PeerAddressIPv6&>(other);
		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
		return std::memcmp(&ip.s6_addr, &other_cast.ip.s6_addr, sizeof(ip.s6_addr)) == 0 && ntohs(port) == ntohs(other_cast.port);
	}


	[[nodiscard]] std::unique_ptr<PeerAddressBase> clone() const override {
		return std::make_unique<PeerAddressIPv6>(*this);
	}


public:
	in6_addr ip = {};
	in_port_t port = {};


	PeerAddressIPv6() = default;


	PeerAddressIPv6(in6_addr ip, in_port_t port): ip(ip), port(port) {
	}


	[[nodiscard]] bool is_connectable() const override {
		return ntohs(port) != 0 && ntohs(port) != 1;
	}


	static PeerAddressIPv6 from_bittorrent(Blob raw) {
		if(raw.size() != 18) {
			throw std::length_error("Invalid PeerAddress length");
		}
		PeerAddressIPv6 addr{};
		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
		std::copy(raw.begin(), raw.begin() + 16, static_cast<unsigned char*>(addr.ip.s6_addr));
		// NOLINTNEXTLINE(hicpp-signed-bitwise)
		addr.port = htons(static_cast<uint16_t>(raw[17]) | (static_cast<uint16_t>(raw[16]) << 8U));
		return addr;
	}


	void decode_msgpack(Blob raw) override {
		*this = from_bittorrent(raw);
		port = bswap_16(port);
	}


	[[nodiscard]] Blob to_msgpack() const override {
		auto native_port = ntohs(port);
		Blob raw(18, 0);
		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
		std::copy(std::begin(ip.s6_addr), std::end(ip.s6_addr), raw.begin());
		raw[16] = native_port & 0xffU; // NOLINT(hicpp-signed-bitwise)
		raw[17] = (native_port >> 8U) & 0xffU; // NOLINT(hicpp-signed-bitwise)
		return raw;
	}


	[[nodiscard]] std::string to_string() const override {
		std::array<char, INET6_ADDRSTRLEN> buffer{};
		inet_ntop(AF_INET6, &ip, buffer.data(), buffer.size());
		return std::string(buffer.data()) + ":" + std::to_string(ntohs(port));
	}


	void visit(const std::function<void(sockaddr*)>& callback) const override {
		sockaddr_in6 addr{AF_INET6, port, 0, ip, 0};
		// sockaddr is forced by socket API
		callback(reinterpret_cast<sockaddr*>(&addr)); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
	}
};


#endif