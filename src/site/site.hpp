#ifndef SITE_SITE_HPP
#define SITE_SITE_HPP


#include "peer/address.hpp"
#include "peer/peer.hpp"
#include "site.hpp"
#include "util/randomset.hpp"
#include <filesystem>
#include <iostream>
#include <map>
#include <queue>
#include <rapidjson/document.h>
#include <set>


class Site;


struct SiteFileDoneEvent {
	std::string path;
};


struct FileRules {
	std::vector<std::string> allowed_signers;
	int signers_required;
};


class SiteEmitter: public uvw::Emitter<SiteEmitter> {
public:
	Site& site;

	explicit SiteEmitter(Site& site);

	friend class Site;
};


struct WorkerCallback {
	int priority;
	std::function<void(std::shared_ptr<Peer>)> f;

	WorkerCallback(int priority, std::function<void(std::shared_ptr<Peer>)> f);
	bool operator<(const WorkerCallback& other) const;
};


class Site: public std::enable_shared_from_this<Site> {
	std::set<PeerAddress> idle_connectable_peers;
	RandomSet<PeerAddress> free_peers;
	std::map<std::string, int> bad_files;
	std::set<std::string> trackers_requesting;
	std::priority_queue<WorkerCallback> worker_callback_queue;
	int updating_peer_count = 0;
	void check_peers();
	void check_peers_tracker(const std::string& tracker, int tries = 8, int numwant = 15);
	void update_file(const std::string& path, const std::string& sha512, size_t size);
	int get_content_modified_time(const std::string& path);

	template<typename... ArgsAll, typename Function, size_t... I>
	void _with_worker_impl(int priority, std::shared_ptr<TaskHandle> handle, std::tuple_element_t<sizeof...(ArgsAll) - 1, std::tuple<ArgsAll...>> callback, Function func, std::index_sequence<I...> index, ArgsAll... args_all);


public:
	std::map<PeerAddress, std::shared_ptr<Peer>> peers;
	SiteEmitter emitter;
	std::string address;

	explicit Site(std::string address);
	Site(const Site&) = delete;
	Site(Site&&) = delete;
	Site& operator=(const Site&) = delete;
	Site& operator=(Site&&) = delete;
	~Site() = default;

	static bool is_valid_address(const std::string& address);
	static bool is_valid_case_sensitive_address(const std::string& address);
	static std::string case_sensitive_address_to_address(std::string address);

	std::string get_case_sensitive_address() const;
	bool need_new_peers();
	void add_peers(const std::vector<PeerAddress>& new_peers, const std::string& origin);
	void update();
	void update_content(const std::string& path, int time_modified = 0);
	std::optional<rapidjson::Document> read_content(const std::string& path);
	std::string get_content_of(std::string path);
	FileRules get_rules(std::string path);
	std::filesystem::path get_path(const std::string& inner_path);

	template<typename... Args, typename Function>
	void with_worker(int priority, const std::string& description, Function func, Args... args);

	void release_worker(const PeerAddress& addr);
	void release_worker_soon(const PeerAddress& addr);
};


#endif