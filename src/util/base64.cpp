#include "base64.hpp"
#include "blob.hpp"


Blob base64::decode(const std::string& code) {
	if(code.size() % 4 != 0) {
		throw std::invalid_argument("Wrong base64-encoded data size");
	}

	std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	Blob buf;
	for(size_t i = 0; i < code.size(); i += 4) {
		size_t a = std::find(alphabet.begin(), alphabet.end(), code[i + 0]) - alphabet.begin();
		size_t b = std::find(alphabet.begin(), alphabet.end(), code[i + 1]) - alphabet.begin();
		size_t c = std::find(alphabet.begin(), alphabet.end(), code[i + 2]) - alphabet.begin();
		size_t d = std::find(alphabet.begin(), alphabet.end(), code[i + 3]) - alphabet.begin();
		if(a == alphabet.size()) {
			throw std::invalid_argument("Invalid character at offset 0 into part");
		}
		if(b == alphabet.size()) {
			throw std::invalid_argument("Invalid character at offset 1 into part");
		}
		if(c == alphabet.size() && code[i + 2] != '=') {
			throw std::invalid_argument("Invalid character at offset 2 into part");
		}
		if(d == alphabet.size() && code[i + 3] != '=') {
			throw std::invalid_argument("Invalid character at offset 3 into part");
		}
		if(code[i + 2] == '=' && code[i + 3] != '=') {
			throw std::invalid_argument("Invalid padding");
		}
		uint32_t n = (a << 18U) | (b << 12U) | (c << 6U) | d;
		buf.push_back(n >> 16U);
		if(code[i + 2] != '=') {
			buf.push_back((n >> 8U) & 0xffU);
			if(code[i + 3] != '=') {
				buf.push_back(n & 0xffU);
			}
		}
	}

	return buf;
}


std::string base64::encode(const Blob& data) {
	std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	std::string res;
	for(size_t i = 0; i < data.size(); i += 3) {
		uint32_t n = static_cast<unsigned char>(data[i]) << 16U;
		if(i + 1 < data.size()) {
			// False positive
			n |= static_cast<unsigned char>(data[i + 1]) << 8U; // NOLINT(hicpp-signed-bitwise)
		}
		if(i + 2 < data.size()) {
			n |= static_cast<unsigned char>(data[i + 2]);
		}
		res.push_back(alphabet[n >> 18U]);
		res.push_back(alphabet[(n >> 12U) & 63U]);
		if(i + 1 < data.size()) {
			res.push_back(alphabet[(n >> 6U) & 63U]);
		} else {
			res.push_back('=');
		}
		if(i + 2 < data.size()) {
			res.push_back(alphabet[n & 63U]);
		} else {
			res.push_back('=');
		}
	}
	return res;
}