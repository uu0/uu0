#ifndef UTIL_BASE58_HPP
#define UTIL_BASE58_HPP


#include "blob.hpp"


namespace base58 {
	std::string encode(const Blob& data);
	std::string encode_check(const Blob& data);
};


#endif