#include "http.hpp"
#include "protocols/ipv4.hpp"
#include "util/blob.hpp"
#include <bencode/bencode.hpp>
#include <boost/variant/get.hpp>
#include <map>


void announce_http(std::array<unsigned char, 20> sha1_raw, int numwant, std::array<char, 20> peer_id, sockaddr addr, const std::string& hostname, const std::string& port, const std::string& hostpath, const std::function<void(std::vector<PeerAddress>)>& callback) {
	auto loop = uvw::Loop::getDefault();

	std::string sha1;
	std::string alphabet = "0123456789abcdef";
	std::string printable = "-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~";
	for(unsigned char c: sha1_raw) {
		if(printable.find(c) == std::string::npos) {
			sha1.push_back('%');
			sha1.push_back(alphabet[c >> 4U]);
			sha1.push_back(alphabet[c & 15U]);
		} else {
			sha1.push_back(c);
		}
	}

	auto path = (
		hostpath.substr(hostpath.find('/')) + // /announce
		"?info_hash=" + sha1 +
		"&peer_id=" + std::string(peer_id.begin(), peer_id.end()) +
		"&port=1" +
		"&uploaded=0" +
		"&downloaded=0" +
		"&left=431102370" +
		"&compact=1" +
		"&numwant=" + std::to_string(numwant) +
		"&event=started"
	);
	auto req = (
		"GET " + path + " HTTP/1.1\r\n" +
		"Connection: close\r\n" +
		"Host: " + hostname + ":" + port + "\r\n" +
		"\r\n"
	);

	auto tcp = loop->resource<uvw::TCPHandle>();
	auto timer = loop->resource<uvw::TimerHandle>();

	tcp->once<uvw::CloseEvent>([timer](const uvw::CloseEvent&, uvw::TCPHandle& tcp) {
		tcp.clear();
		timer->close();
	});

	tcp->once<uvw::ErrorEvent>([](const uvw::ErrorEvent&, uvw::TCPHandle& tcp) {
		tcp.close();
	});

	tcp->once<uvw::ConnectEvent>([timer, req, callback](const uvw::ConnectEvent&, uvw::TCPHandle& tcp) {
		auto response = std::make_shared<std::string>();
		tcp.on<uvw::DataEvent>([response](const uvw::DataEvent& data, uvw::TCPHandle&) {
			*response += std::string(data.data.get(), data.length);
		});
		tcp.once<uvw::EndEvent>([timer, response, callback](const uvw::EndEvent&, uvw::TCPHandle& tcp) {
			tcp.close();

			auto i = response->find("\r\n\r\n");
			if(i == std::string::npos) {
				return;
			}
			*response = response->substr(i + 4);

			try {
				auto obj = bencode::decode(*response);
				auto obj_dict = boost::get<bencode::dict>(obj);
				if(obj_dict.count("peers") == 0) {
					return;
				}
				auto peers_dict = boost::get<std::string>(obj_dict["peers"]);

				if(peers_dict.size() % 6 != 0) {
					return;
				}
				std::vector<PeerAddress> peers;
				auto it = peers_dict.begin();
				for(size_t i = 0; i < peers_dict.size(); i += 6) {
					Blob slice(6, 0);
					// We've already verified that peers_dict 
					// NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
					std::copy(it + i, it + i + 6, slice.begin());
					peers.emplace_back(PeerAddressIPv4::from_bittorrent(slice));
				}
				callback(peers);
			} catch(std::invalid_argument&) {
			}
		});

		// char[] is forced by uvw, wrapping it in unique_ptr is the best we can achieve
		// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
		auto req_buf = std::make_unique<char[]>(req.size());
		std::copy(req.begin(), req.end(), req_buf.get());
		tcp.write(std::move(req_buf), req.size());

		tcp.read();
	});
	tcp->connect(addr);


	timer->once<uvw::CloseEvent>([](const uvw::CloseEvent&, uvw::TimerHandle& timer) {
		timer.clear();
	});

	timer->once<uvw::TimerEvent>([tcp](const uvw::TimerEvent&, uvw::TimerHandle& timer) {
		tcp->close();
		timer.close();
	});

	timer->start(std::chrono::milliseconds{15000}, std::chrono::milliseconds{15000});
}