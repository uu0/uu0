#ifndef PEER_CONNECTION_HPP
#define PEER_CONNECTION_HPP


#include "address.hpp" // IWYU pragma: keep
// IWYU pragma: no_include "peer/address.hpp"
#include "message.hpp"
#include "util/deleter.hpp"
#include <map>
#include <msgpack.hpp>
#include <ofats/invocable.h>


class Connection;


class ConnectionManager {
	std::map<PeerAddress, std::shared_ptr<Connection>> connections;
	void remove(const std::shared_ptr<Connection>& connection);

public:
	std::shared_ptr<Connection> add(const PeerAddress& addr);
	std::weak_ptr<Connection> get(const PeerAddress& addr);

	friend class Connection;
};


struct ConnectionEstablishedEvent {
};

struct ConnectionClosedEvent {
};


class ConnectionEmitter: public uvw::Emitter<ConnectionEmitter> {
	friend class ::Connection;
};


class Connection: public std::enable_shared_from_this<Connection> {
	ConnectionManager& manager;
	PeerAddress addr;
	std::shared_ptr<uvw::TCPHandle> tcp;
	std::map<int, ofats::any_invocable<void(msgpack::object)>> response_queue;
	msgpack::unpacker unpacker;
	int next_req_id = 1;
	void connect();
	void handle_connection();
	void do_handshake();
	void handle_message(msgpack::object obj);
	void handle_handshake_response(msgpack::object obj);

public:
	ConnectionEmitter emitter;
	std::optional<ConnectionHandshake> handshake;
	bool established;

	Connection(ConnectionManager& manager, PeerAddress addr);
	void close();
	int get_req_id();

	template<typename T> void send(T obj) {
		msgpack::sbuffer buf;
		msgpack::pack(&buf, obj);

		auto size = buf.size();
		auto ptr = buf.release();
		// char[] is forced by uvw
		// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
		tcp->write(std::unique_ptr<char[], decltype(&free_deleter)>(ptr, &free_deleter), size);
	}

	template<typename T> void send(T obj, ofats::any_invocable<void(msgpack::object)>&& callback) {
		response_queue.emplace(obj.req_id, std::move(callback));
		send(obj);
	}

	friend class ConnectionManager;
};


static ConnectionManager connection_manager;


#endif