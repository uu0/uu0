#include "address.hpp"
#include "util/blob.hpp"
#include <typeindex>


PeerAddress::PeerAddress(const PeerAddress& other): inner(other.inner->clone()) {
}


PeerAddress::PeerAddress(PeerAddress&& other) noexcept: inner(std::move(other.inner)) {
}


PeerAddress& PeerAddress::operator=(const PeerAddress& other) {
	if(this != &other) {
		inner = other.inner->clone();
	}
	return *this;
}


PeerAddress& PeerAddress::operator=(PeerAddress&& other) noexcept {
	inner = std::move(other.inner);
	return *this;
}


bool PeerAddress::is_connectable() const {
	return inner->is_connectable();
}


std::string PeerAddress::to_string() const {
	return inner->to_string();
}


bool PeerAddress::operator<(const PeerAddress& other) const {
	return *inner < *other.inner;
}


bool PeerAddress::operator==(const PeerAddress& other) const {
	return *inner == *other.inner;
}


void PeerAddress::visit(const std::function<void(sockaddr*)>& callback) const {
	inner->visit(callback);
}


bool operator<(const PeerAddressBase& a, const PeerAddressBase& b) {
	auto idx_a = std::type_index(typeid(a));
	auto idx_b = std::type_index(typeid(b));
	if(idx_a != idx_b) {
		return idx_a < idx_b;
	}
	return a.is_less(b);
}


bool operator==(const PeerAddressBase& a, const PeerAddressBase& b) {
	auto idx_a = std::type_index(typeid(a));
	auto idx_b = std::type_index(typeid(b));
	return idx_a == idx_b && a.is_equal(b);
}


msgpack::object const& PeerAddressBase::msgpack_unpack(msgpack::object const& obj) {
	if(obj.type != msgpack::type::STR) {
		throw msgpack::type_error();
	}
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
	Blob raw(obj.via.str.size, 0);
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access, cppcoreguidelines-pro-bounds-pointer-arithmetic)
	std::copy(obj.via.str.ptr, obj.via.str.ptr + obj.via.str.size, raw.begin());
	try {
		decode_msgpack(raw);
	} catch(std::length_error&) {
		throw msgpack::type_error();
	}
	return obj;
}