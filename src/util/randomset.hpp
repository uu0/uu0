#ifndef UTIL_RANDOMSET_HPP
#define UTIL_RANDOMSET_HPP


#include "util/rand.hpp"
#include <map>
#include <random>
#include <set>


template<typename T> class RandomSet {
	std::set<T> set;
	std::map<T, size_t> id_of_key;
	std::vector<T> keys;

	void add_key(const T& key) {
		if(!set.count(key)) {
			id_of_key.insert({key, keys.size()});
			keys.push_back(key);
		}
	}

	void remove_key(const T& key) {
		if(set.count(key)) {
			id_of_key[keys.back()] = id_of_key[key];
			std::swap(keys[id_of_key[key]], keys.back());
			id_of_key.erase(key);
			keys.pop_back();
		}
	}

public:
	using key_type = T;
	using value_type = T;
	using size_type = typename std::set<T>::size_type;
	using difference_type = typename std::set<T>::difference_type;
	using key_compare = typename std::set<T>::key_compare;
	using value_compare = typename std::set<T>::value_compare;
	using allocator_type = typename std::set<T>::allocator_type;
	using reference = T&;
	using const_reference = const T&;
	using pointer = typename std::set<T>::pointer;
	using const_pointer = typename std::set<T>::const_pointer;
	using iterator = typename std::set<T>::iterator;
	using const_iterator = typename std::set<T>::const_iterator;
	using reverse_iterator = typename std::set<T>::reverse_iterator;
	using const_reverse_iterator = typename std::set<T>::const_reverse_iterator;

	[[nodiscard]] T get_random_key() const {
		if(keys.empty()) {
			throw std::range_error("Empty RandomSet");
		}
		std::uniform_int_distribution<size_t> rnd(0, keys.size() - 1);
		return keys[rnd(random_source::gen) % keys.size()];
	}

	[[nodiscard]] iterator begin() {
		return set.begin();
	}
	[[nodiscard]] const_iterator begin() const {
		return set.begin();
	}
	[[nodiscard]] const_iterator cbegin() const {
		return set.cbegin();
	}
	[[nodiscard]] iterator end() {
		return set.end();
	}
	[[nodiscard]] const_iterator end() const {
		return set.end();
	}
	[[nodiscard]] const_iterator cend() const {
		return set.cend();
	}
	[[nodiscard]] reverse_iterator rbegin() {
		return set.rbegin();
	}
	[[nodiscard]] const_reverse_iterator rbegin() const {
		return set.rbegin();
	}
	[[nodiscard]] const_reverse_iterator crbegin() const {
		return set.crbegin();
	}
	[[nodiscard]] reverse_iterator rend() {
		return set.rend();
	}
	[[nodiscard]] const_reverse_iterator rend() const {
		return set.rend();
	}
	[[nodiscard]] const_reverse_iterator crend() const {
		return set.crend();
	}

	[[nodiscard]] bool empty() const {
		return set.empty();
	}
	[[nodiscard]] size_type size() const {
		return set.size();
	}
	[[nodiscard]] size_type max_size() const {
		return set.max_size();
	}

	void clear() {
		set.clear();
		id_of_key.clear();
		keys.clear();
	}

	std::pair<iterator, bool> insert(const T& value) {
		add_key(value);
		return set.insert(value);
	}
	template<typename P> std::pair<iterator, bool> insert(P&& value) {
		add_key(value);
		return set.insert(value);
	}
	iterator insert(const_iterator hint, const T& value) {
		add_key(value);
		return set.insert(hint, value);
	}
	template<typename P> iterator insert(const_iterator hint, P&& value) {
		add_key(value);
		return set.insert(hint, value);
	}
	template<typename InputIt> void insert(InputIt first, InputIt last) {
		for(auto it = first; it != last; ++it) {
			add_key(*it);
		}
		set.insert(first, last);
	}
	void insert(std::initializer_list<T> ilist) {
		for(auto key: ilist) {
			add_key(key);
		}
		set.insert(ilist);
	}

	template<typename... Args> std::pair<iterator, bool> emplace(Args&&... args) {
		auto res = set.emplace(args...);
		if(res->second) {
			add_key(*res->first);
		}
		return res;
	}
	template<typename... Args> std::pair<iterator, bool> emplace_hint(const_iterator hint, Args&&... args) {
		auto res = set.emplace_hint(hint, args...);
		if(res->second) {
			add_key(*res->first);
		}
		return res;
	}

	iterator erase(const_iterator position) {
		remove_key(*position);
		return set.erase(position);
	}
	iterator erase(const_iterator first, const_iterator last) {
		for(auto it = first; it != last; ++it) {
			remove_key(*it);
		}
		return set.erase(first, last);
	}
	size_type erase(const T& key) {
		remove_key(key);
		return set.erase(key);
	}

	void swap(RandomSet& other) {
		std::swap(set, other.set);
		std::swap(id_of_key, other.id_of_key);
		std::swap(keys, other.keys);
	}

	[[nodiscard]] size_type count(const T& key) const {
		return set.count(key);
	}
	[[nodiscard]] iterator find(const T& key) {
		return set.find(key);
	}
	[[nodiscard]] const_iterator find(const T& key) const {
		return set.find(key);
	}
	[[nodiscard]] std::pair<iterator, iterator> equal_range(const T& key) {
		return set.equal_range(key);
	}
	[[nodiscard]] std::pair<const_iterator, const_iterator> equal_range(const T& key) const {
		return set.equal_range(key);
	}
	[[nodiscard]] iterator lower_bound(const T& key) {
		return set.lower_bound(key);
	}
	[[nodiscard]] const_iterator lower_bound(const T& key) const {
		return set.lower_bound(key);
	}
	[[nodiscard]] iterator upper_bound(const T& key) {
		return set.upper_bound(key);
	}
	[[nodiscard]] const_iterator upper_bound(const T& key) const {
		return set.upper_bound(key);
	}

	[[nodiscard]] key_compare key_comp() const {
		return set.key_comp();
	}
	[[nodiscard]] value_compare value_comp() const {
		return set.value_comp();
	}
};


#endif