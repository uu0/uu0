#ifndef UTIL_TEMPFILE_HPP
#define UTIL_TEMPFILE_HPP


#include <filesystem>


class TempFile {
	bool existing;
	std::filesystem::path path;

public:
	TempFile();
	TempFile(const TempFile&) = delete;
	TempFile(TempFile&& other) noexcept;
	~TempFile();

	TempFile& operator=(const TempFile&) = delete;
	TempFile& operator=(TempFile&& other) noexcept;

	[[nodiscard]] const std::filesystem::path& get_path() const;

	void remove();
	bool move(const std::filesystem::path& to);
};


#endif