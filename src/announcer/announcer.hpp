#ifndef ANNOUNCER_ANNOUNCER_HPP
#define ANNOUNCER_ANNOUNCER_HPP


#include "peer/address.hpp"


class Announcer {
public:
	std::vector<std::string> trackers;
	std::array<char, 20> peer_id;

	Announcer();
	void add_tracker(const std::string& tracker);
	void announce(const std::string& subject, int numwant, const std::string& tracker, const std::function<void(std::vector<PeerAddress>)>& callback);
};

extern Announcer announcer;


#endif