// Forced by rapidjson
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define RAPIDJSON_HAS_STDSTRING 1
#include "json.hpp"
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>


JSONObject::JSONObject(std::initializer_list<std::pair<const std::string, JSON>> attributes): attributes(attributes) {
}

JSONArray::JSONArray(std::initializer_list<JSON> list): list(list) {
}

JSONString::JSONString(std::string str): str(std::move(str)) {
}
JSONString::JSONString(const char* str): str(str) {
}

JSONInteger::JSONInteger(int64_t integer): integer(integer) {
}


JSON& JSONObject::operator[](const std::string& attr) {
	return attributes[attr];
}


rapidjson::Value _encode_json_impl(JSONObject&& object, rapidjson::Document& doc) {
	auto& allocator = doc.GetAllocator();

	rapidjson::Value obj;
	obj.SetObject();
	for(auto it = object.attributes.begin(); it != object.attributes.end();) {
		auto attr = object.attributes.extract(it++);
		auto value_enc = encode_json(std::move(attr.mapped()), doc);
		// TODO(ivanq): check if we can move std::string into rapidjson::Value
		obj.AddMember(rapidjson::Value{attr.key(), allocator}, std::move(value_enc), allocator);
	}
	return obj;
}


rapidjson::Value _encode_json_impl(JSONArray&& array, rapidjson::Document& doc) {
	auto& allocator = doc.GetAllocator();

	rapidjson::Value obj;
	obj.SetArray();
	for(auto&& value: array.list) {
		auto value_enc = std::visit([&doc](auto&& obj) {
			return encode_json(std::forward<decltype(obj)>(obj), doc);
		}, std::move(value));
		obj.PushBack(std::move(value_enc), allocator);
	}
	return obj;
}


rapidjson::Value _encode_json_impl(JSONString&& string, rapidjson::Document& doc) {
	rapidjson::Value obj;
	obj.SetString(string.str, doc.GetAllocator());
	return obj;
}


rapidjson::Value _encode_json_impl(JSONInteger&& integer, rapidjson::Document&) {
	rapidjson::Value obj;
	obj.SetInt(integer.integer);
	return obj;
}


rapidjson::Value _encode_json_impl(JSONNull&&, rapidjson::Document&) {
	return {};
}


template<> rapidjson::Value encode_json<JSON>(JSON&& json, rapidjson::Document& doc) {
	return std::visit([&doc](auto&& json) {
		static_assert(std::is_same_v<decltype(json), std::decay_t<decltype(json)>&&>);
		return _encode_json_impl(std::forward<decltype(json)>(json), doc);
	}, std::move(json));
}


std::string stringify_json(JSON&& json) {
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	rapidjson::Document doc;
	// These false positives are because of rapidjson internal hacks
	// NOLINTNEXTLINE(clang-analyzer-core.uninitialized.Assign, clang-analyzer-core.UndefinedBinaryOperatorResult)
	encode_json(std::move(json), doc).Accept(writer);
	return buffer.GetString();
}