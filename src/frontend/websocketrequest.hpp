#ifndef FRONTEND_WEBSOCKETREQUEST_HPP
#define FRONTEND_WEBSOCKETREQUEST_HPP


#include "httprequest.hpp"
#include <websocket-parser/websocket_parser.h>


class WebSocketRequest {
	void send(websocket_flags flags, const char* data, size_t size) const;

public:
	HTTPRequest& http;


	explicit WebSocketRequest(HTTPRequest& http);

	void send(const std::string& data) const;
	void send_pong(const std::string& data) const;
	void send_close() const;
};


#endif