#include "msgpack.hpp"
#include <msgpack.hpp>


void fix_msgpack_object(msgpack::object& obj) {
	// All the nolint's are because of C-style msgpack::object implementation
	switch(obj.type) {
		case msgpack::type::BIN:
			static_assert(std::is_same_v<decltype(msgpack::object_str::size), decltype(msgpack::object_bin::size)>);
			static_assert(std::is_same_v<decltype(msgpack::object_str::ptr), decltype(msgpack::object_bin::ptr)>);
			static_assert(sizeof(msgpack::object_str) == sizeof(msgpack::object_bin));
			obj.type = msgpack::type::STR;
			break;

		case msgpack::type::ARRAY:
			for(size_t i = 0; i < obj.via.array.size; i++) { // NOLINT(cppcoreguidelines-pro-type-union-access)
				fix_msgpack_object(obj.via.array.ptr[i]); // NOLINT(cppcoreguidelines-pro-type-union-access, cppcoreguidelines-pro-bounds-pointer-arithmetic)
			}
			break;

		case msgpack::type::MAP:
			for(size_t i = 0; i < obj.via.map.size; i++) { // NOLINT(cppcoreguidelines-pro-type-union-access)
				fix_msgpack_object(obj.via.map.ptr[i].key); // NOLINT(cppcoreguidelines-pro-type-union-access, cppcoreguidelines-pro-bounds-pointer-arithmetic)
				fix_msgpack_object(obj.via.map.ptr[i].val); // NOLINT(cppcoreguidelines-pro-type-union-access, cppcoreguidelines-pro-bounds-pointer-arithmetic)
			}
			break;

		default:
			break;
	}
}