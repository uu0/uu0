const BROWSERS = ["firefox", "tor-browser", "chromium", "chrome", "edge", "yandex-browser", "opera", "safari"];


// Detect browser by feature support
let mainBrowsers;
if(window.InstallTrigger) {
	mainBrowsers = ["firefox", "tor-browser"];
} else if(navigator.userAgent.indexOf("OPR") > -1) {
	mainBrowsers = ["opera"];
} else if(navigator.vendor && navigator.vendor.indexOf("Apple") > -1) {
	mainBrowsers = ["safari"];
} else if(Array.from(navigator.plugins).some(plugin => plugin.name === "Chromium PDF Viewer")) {
	mainBrowsers = ["chromium", "chrome"];
} else {
	mainBrowsers = ["chrome", "edge", "yandex-browser"];
}

for(const browser of BROWSERS) {
	const node = document.querySelector(`.browser-${browser}`);
	node.innerHTML = `
		<div class="browser-logo-container">
			<img src="/ZeroNet-Internal/assets/frontend/logos/${browser}.png" />
		</div>
	` + node.innerHTML;
}

for(const browser of mainBrowsers) {
	const node = document.querySelector(`.browser-${browser}`);
	document.querySelector(".main-browsers").appendChild(node);
}


function installFirefox() {
	InstallTrigger.install({
		"uu0": `${location.href}ZeroNet-Internal/assets/frontend/uu0-firefox.xpi`
	});
}

function installChromium() {
	document.querySelector(".popup-main").style.display = "none";
	document.querySelector(".popup-chromium").style.display = "";
}

function installChrome() {
	window.open("https://chrome.google.com/webstore", null, `width=${screen.availWidth},height=${screen.availHeight}`);
}

function installOpera() {
	window.open("https://addons.opera.com/en/extensions/", null, `width=${screen.availWidth},height=${screen.availHeight}`);
}

function installSafari() {
}